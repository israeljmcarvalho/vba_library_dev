Option Explicit

' This is the Sub that opens external files and reads in the contents.
' In this way, you can have separate files for data and libraries of functions
Sub Include(yourFile)

  Dim oFSO, oFileBeingReadIn   ' define Objects
  Dim sFileContents            ' define Strings
 
  Set oFSO = CreateObject("Scripting.FileSystemObject")
  Set oFileBeingReadIn = oFSO.OpenTextFile("C:\Users\icarvalho\Documents\VBA\Git\vbscripts\VBS Library.vbs", 1)
   
  sFileContents = oFileBeingReadIn.ReadAll
  oFileBeingReadIn.Close
  ExecuteGlobal sFileContents
  
End Sub

Include "VBS Library"

'================================================================================================================================

Dim sScriptdir
Dim sCommit
Dim bolBashScriptPause

	sScriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
	sCommit = InputBox("Type the commit here", "COMMIT", "Update - " & FormatDateTime(Now, vbGeneralDate))
	bolBashScriptPause = False
	Call GitPush_Lib (sScriptdir, sCommit, bolBashScriptPause)