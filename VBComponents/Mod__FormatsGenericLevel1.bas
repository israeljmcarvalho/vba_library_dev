Attribute VB_Name = "Mod__FormatsGenericLevel1"
Option Explicit

Sub FormatGenericFonts_Lib(sExecutionID, sUser, ParametroRecebido)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub FormatGenericFonts_Lib(sExecutionID, sUser, ParametroRecebido)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & "RANGE " & ParametroRecebido.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    
    With ParametroRecebido.Font
        .Name = "Calibri Light"
        .Size = 10
        .ColorIndex = 1
    End With
    
    ParametroRecebido.EntireRow.RowHeight = 18
    ParametroRecebido.Interior.ColorIndex = 0
    ParametroRecebido.VerticalAlignment = xlCenter
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
     
End Sub

Sub FormatGenericBorders_Lib(sExecutionID, sUser, ParametroRecebido)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub FormatGenericBorders_Lib(sExecutionID, sUser, ParametroRecebido)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & "RANGE " & ParametroRecebido.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    With ParametroRecebido
        .Borders(xlDiagonalDown).LineStyle = xlNone
        .Borders(xlDiagonalUp).LineStyle = xlNone
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).ColorIndex = xlAutomatic
        .Borders(xlEdgeLeft).TintAndShade = 0
        .Borders(xlEdgeLeft).Weight = xlHairline
        .Borders(xlEdgeTop).LineStyle = xlContinuous
        .Borders(xlEdgeTop).ColorIndex = xlAutomatic
        .Borders(xlEdgeTop).TintAndShade = 0
        .Borders(xlEdgeTop).Weight = xlHairline
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).ColorIndex = xlAutomatic
        .Borders(xlEdgeBottom).TintAndShade = 0
        .Borders(xlEdgeBottom).Weight = xlHairline
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).ColorIndex = xlAutomatic
        .Borders(xlEdgeRight).TintAndShade = 0
        .Borders(xlEdgeRight).Weight = xlHairline
        .Borders(xlInsideVertical).LineStyle = xlContinuous
        .Borders(xlInsideVertical).ColorIndex = xlAutomatic
        .Borders(xlInsideVertical).TintAndShade = 0
        .Borders(xlInsideVertical).Weight = xlHairline
        .Borders(xlInsideHorizontal).LineStyle = xlContinuous
        .Borders(xlInsideHorizontal).ColorIndex = xlAutomatic
        .Borders(xlInsideHorizontal).TintAndShade = 0
        .Borders(xlInsideHorizontal).Weight = xlHairline
    End With

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub FormatGenericGrayFonts_Lib(sExecutionID, sUser, ParametroRecebido)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub FormatGenericGrayFonts_Lib(sExecutionID, sUser, ParametroRecebido)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & "RANGE " & ParametroRecebido.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    With rnWithoutHeader
        .Font.ColorIndex = 16
        .Interior.ColorIndex = 2
    End With

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub FormatsGenericTextNumberFormat_Lib(sExecutionID, sUser, ParametroRecebido)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub FormatGenericSelectRange_Lib(sExecutionID, sUser, ParametroRecebido)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & "RANGE " & ParametroRecebido.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    ParametroRecebido.NumberFormat = "@"

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub




























