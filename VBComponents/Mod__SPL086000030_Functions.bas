Attribute VB_Name = "Mod__SPL086000030_Functions"
Option Explicit

Function GetRowToPasteCompany2Data_Anterior(sExecutionID) As Long

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function GetRowToPasteCompany2Data(sExecutionID)"
sFunctionParameters = sExecutionID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    'Defini��o Row Cabe�alho Argentina
    GetRowToPasteCompany2Data = [A100000].End(xlUp).Offset(1, 0).Row

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


Sub SetYearMonth_Lib_Anterior(sExecutionID, sUser, iYear, iMonth)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub SetYearMonth_Lib(sExecutionID, sUser, iYear, iMonth)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & iMonth
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsDatabase.Range(wsDatabase.[A2], wsDatabase.[C1000000].End(xlUp).Offset(0, -2)).Value = iYear
    wsDatabase.Range(wsDatabase.[B2], wsDatabase.[A1000000].End(xlUp).Offset(0, 1)).Value = iMonth

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub




