Attribute VB_Name = "Mod___References"
Option Explicit

Sub RefreshLinkedReferences_Lib(wb)
    
    Debug.Print String(40, "=") & Chr(13) & "Rever implementa��o desta fun��o. Creio que ela deveria estar no ThisWorkbook de cada automa��o." & Chr(13) & String(40, "=")
    
    Call RemoveMissingReferences_Lib(wb)
    Call AddGeneralFunctionsDevReference_Lib(wb)
    
End Sub

Sub RemoveMissingReferences_Lib(wb)

Dim vbProj As VBProject
Dim chkRef As Reference

Debug.Print String(40, "=") & Chr(13) & "Rever implementa��o desta fun��o. Creio que ela deveria estar no ThisWorkbook de cada automa��o." & Chr(13) & String(40, "=")

Set vbProj = wb.VBProject

For Each chkRef In vbProj.References
   If chkRef.IsBroken Then
     'Debug.Print chkRef.Name
      vbProj.References.Remove chkRef
   End If
Next

End Sub

Sub AddGeneralFunctionsDevReference_Lib(wb)

Debug.Print String(40, "=") & Chr(13) & "Rever implementa��o desta fun��o. Creio que ela deveria estar no ThisWorkbook de cada automa��o." & Chr(13) & String(40, "=")

Dim vbProj As VBProject

Set vbProj = wb.VBProject

On Error Resume Next
    vbProj.References.AddFromFile "\\brscjoi-fp01\groups\SSC\SSC AUTOMATION\VBA\Add-ins\General Functions_dev.xlam"
On Error GoTo 0
Set vbProj = Nothing

End Sub
