Attribute VB_Name = "Mod__Regex"
Sub RegexTestingAPattern()

Dim stringOne As String
Dim regexOne As Object

Set regexOne = New RegExp

regexOne.Pattern = "f....a"

stringOne = "000111fjo88a8"

Debug.Print regexOne.Test(stringOne)

End Sub

Sub Del_Rows()

  Application.ScreenUpdating = False
  
  With Range("F1:F2000")
    .Formula = "=IF(COUNTBLANK(A1:F1)=6,"""",1)"
    .Value = .Value
    .SpecialCells(xlBlanks).EntireRow.Delete
    .ClearContents
  End With
  
  Application.ScreenUpdating = True
  
End Sub

Function FindReplaceRegex(rng As Range, reg_exp As String, replace As String)
    
Dim myRegExp

    
Set myRegExp = New RegExp
myRegExp.IgnoreCase = False
myRegExp.Global = True
myRegExp.Pattern = reg_exp

FindReplaceRegex = myRegExp.replace(rng.Value, replace)
    
End Function

Sub TesteRegex()

Dim rn As Range


Set rn = [A:A]

MsgBox FindReplaceRegex(rn, "T", "XXX")

End Sub
