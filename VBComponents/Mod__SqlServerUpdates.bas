Attribute VB_Name = "Mod__SqlServerUpdates"
Option Explicit

Sub UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sTestStatus
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Update T_AutomationsLogs Set [TestStatus] = '" & sTestStatus & "' Where [ExecutionID] = '" & sExecutionID & "'"
    
    Call OpenSqlServerConection_Lib
    Set rs = conn.Execute(sSqlQuery)
    Call CloseSqlServerConection_Lib

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub UpdateExecutionEndTime_Lib(sExecutionID, sUser, sAutomationID)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub UpdateExecutionEndTime_Lib(sExecutionID, sUser, sAutomationID)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sAutomationID
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_UpdateExecutionEndTime @ExecutionID = '" & sExecutionID & "'"
    
    Call OpenSqlServerConection_Lib
    Set rs = conn.Execute(sSqlQuery)
    Call CloseSqlServerConection_Lib

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub
