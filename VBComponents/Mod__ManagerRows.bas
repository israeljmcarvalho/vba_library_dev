Attribute VB_Name = "Mod__ManagerRows"
Option Explicit

Sub DeleteBlankRows_Lib(sExecutionID, sUser, rn, iColIndex)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub DeleteBlankRows_Lib(sExecutionID, sUser, wb, ws, rn)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address & ", " & iColIndex
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    rn.AutoFilter Field:=iColIndex, Criteria1:="="
    Call ResizeTableRemovingHeader_Lib(sExecutionID, sUser, rn)
    ActiveSheet.ShowAllData

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


