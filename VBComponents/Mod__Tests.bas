Attribute VB_Name = "Mod__Tests"
Option Explicit

Function CheckThereAreFormulasWithErrors_Lib(sExecutionID, sUser, sMainWokbook, sMainWorksheet, rnRange, sLookAtWorkbookName, wsLookAtWorksheetName) As Boolean

Dim rnWithError                         As Range
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function CheckThereAreFormulasWithErrors_Lib(sExecutionID, sUser, sMainWokbook, sMainWorksheet, rnRange, sLookAtWorkbookName, wsLookAtWorksheetName) As Boolean"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sMainWokbook & ", " & sMainWorksheet & ", RANGE " & rnRange.Address & ", " & sLookAtWorkbookName & ", SHEET " & wsLookAtWorksheetName.Name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    On Error Resume Next
    ActiveSheet.ShowAllData
    On Error GoTo 0
    
    On Error GoTo err
    Set rnWithError = rnRange.SpecialCells(xlCellTypeFormulas, 16)
    rnWithError.Select
    rnWithError.Interior.ColorIndex = 6
    
    Debug.Print "There is an error in the formula in the cell below: " & Chr(13) & _
        "----------------------------------" & Chr(13) & _
        "MainWorkbook: " & sMainWokbook & Chr(13) & _
        "Look At Workbook: " & sLookAtWorkbookName & Chr(13) & _
        "Look At Worksheet: " & wsLookAtWorksheetName.Name & Chr(13) & _
        "Look At Range: " & rnRange.Address & Chr(13) & _
        "Cell with error: " & rnWithError.Address & Chr(13)
    
Done:
        CheckThereAreFormulasWithErrors_Lib = True
        Exit Function
    
err:
        CheckThereAreFormulasWithErrors_Lib = False
        
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Function

Function GetTestResult_Lib(sExecutionID, sUser)

'Retorna PASS or FAILED conforma field [TestStatus] da tabela T_AutomationsLogs
sSqlQuery = "Execute SP_GetTestResult @TestID = '" & sExecutionID & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)
Call CloseSqlServerConection_Lib

End Function

