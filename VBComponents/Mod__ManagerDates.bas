Attribute VB_Name = "Mod__ManagerDates"
Option Explicit

Function FunctionGetMonthName_Lib(sExecutionID, sUser, iMonth) As String

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function FunctionGetMonthName_Lib(sExecutionID, sUser, iMonth) As String"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & iMonth
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Select Case iMonth
    
        Case 1
            FunctionGetMonthName_Lib = "JAN"
        Case 2
            FunctionGetMonthName_Lib = "FEB"
        Case 3
            FunctionGetMonthName_Lib = "MAR"
        Case 4
            FunctionGetMonthName_Lib = "APR"
        Case 5
            FunctionGetMonthName_Lib = "MAY"
        Case 6
            FunctionGetMonthName_Lib = "JUN"
        Case 7
            FunctionGetMonthName_Lib = "JUL"
        Case 8
            FunctionGetMonthName_Lib = "AUG"
        Case 9
            FunctionGetMonthName_Lib = "SEP"
        Case 10
            FunctionGetMonthName_Lib = "OCT"
        Case 11
            FunctionGetMonthName_Lib = "NOV"
        Case 12
            FunctionGetMonthName_Lib = "DEC"
    End Select

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function SetDatetimeInsert_Lib(sExecutionID, sUser) As String

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function SetDatetimeInsert_Lib(sExecutionID, sUser) As String"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    SetDatetimeInsert_Lib = Chr(39) & Format(Now(), "yyyy-mm-dd hh:mm:ss") & Chr(39)
    'Debug.Print SetDatetimeInsert_Lib & Chr(13) & "2021-08-30 11:33:25.610"
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Sub SetYearMonth_Lib_Anterior(sExecutionID, sUser, iYear, iMonth)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub SetYearMonth_Lib(sExecutionID, sUser, iYear, iMonth)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sExecutionID & ", " & iMonth
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    wsDatabase.Range(wsDatabase.[A2], wsDatabase.[C1000000].End(xlUp).Offset(0, -2)).Value = iYear
    wsDatabase.Range(wsDatabase.[B2], wsDatabase.[A1000000].End(xlUp).Offset(0, 1)).Value = iMonth

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub
