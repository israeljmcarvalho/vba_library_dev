Attribute VB_Name = "Mod__ExportVbComponents"
Option Explicit

Function GetGitPath_Lib(sAutomationID, sUser)

Dim sSqlQuery                         As String

sSqlQuery = "Select [GitRepositoryPath] from T_Automations Where [ID] ='" & sAutomationID & "'"

Call OpenSqlServerConection_Lib

    Set rs = conn.Execute(sSqlQuery)
    
    If rs.EOF = False Then
        GetGitPath_Lib = rs.Fields(0)
    End If
        
Call CloseSqlServerConection_Lib


End Function

Function ExportVbComponents_Lib(wb, GitPath, objVBPrj)


Dim objVBCom            As VBIDE.VBComponent

'Debug.Print objVBPrj.Name
For Each objVBCom In objVBPrj.VBComponents

    Select Case CStr(objVBCom.Type)
    
        'Case "100" 'Microsoft Excel Objects. Sheet modules or the ThisWorkbook module.
            
        '    wb.VBProject.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".cls"
        
        Case "1" 'Regular Modules
            
            'wb.VBProject.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".bas"
            objVBPrj.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".bas"
        
        Case "2" 'Class Modules
            
            'wb.VBProject.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".cls"
            objVBPrj.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".cls"
        
        Case "3" 'UserForms
            
            'wb.VBProject.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".frm"
            objVBPrj.VBComponents(objVBCom.Name).Export GitPath & "\" & objVBCom.Name & ".frm"
        
    End Select
   
Next


End Function
