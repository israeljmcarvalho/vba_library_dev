Attribute VB_Name = "Mod__ManagerRanges"
Option Explicit

Function GetHeader_Lib(sExecutionID, sUser, rn) As Range

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Function GetHeader_Lib(sExecutionID, sUser, rn) As Range"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Set GetHeader_Lib = rn.Resize(1, rn.Columns.Count)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn) As Range

Dim lCountOfRecords                     As Long
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn) As Range"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    lCountOfRecords = GetCountOfRecords_Lib(sExecutionID, sUser, rn) - 1
    
    If lCountOfRecords >= 1 Then
        Set GetRangeWithoutHeader_Lib = rn.Offset(1, 0).Resize(rn.Rows.Count - 1, rn.Columns.Count)
    Else
        Set GetRangeWithoutHeader_Lib = Nothing
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Function

Sub ResizeTableRemovingHeader_Lib(sExecutionID, sUser, rn)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub ResizeTableRemovingHeader_Lib(sExecutionID, sUser, rn)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    rn.Offset(1, 0).Resize(rn.Rows.Count - 1, rn.Columns.Count).SpecialCells(xlCellTypeVisible).EntireRow.Delete

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function ConvertRangeToR1C1_Lib(rnRange) As String

'Como chamar a fun��o
'Set rnRange = [B1]
'sString = ConvertRangeToR1C1(rnRange)
'Retorno esperado R1C2:R1C2

ConvertRangeToR1C1_Lib = rnRange.Address(ReferenceStyle:=xlR1C1)

End Function


Function ConvertStringToR1C1_Lib(sExecutionID, sUser, rnRange) As Range

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Function ConvertStringToR1C1_Lib(sExecutionID, sUser, rnRange) As Range"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rnRange
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    'Como chamar a fun��o
    'sTargetCell = "$G$2"
    'Set rnRange = ConvertStringToR1C1(sTargetCell)
    'rnRange.Select
    'Retorno esperado R1C2:R1C2
    
    Set ConvertStringToR1C1_Lib = Range(replace(rnRange, "$", ""))
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function
