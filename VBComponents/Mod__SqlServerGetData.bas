Attribute VB_Name = "Mod__SqlServerGetData"
Option Explicit

Sub LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

Dim aQuery                              As Variant
Dim iCountOfRecords                     As Integer
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSqlQuery & ", " & sSheetName & ", " & sTargetCell & ", " & sRangeName
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    

    '----------------------------------------------------------------------------------------------------------------
    'Insert Data without header

    Call OpenSqlServerConection_Lib
    
        rs.Open sSqlQuery, conn
         
        If rs.EOF = True Then
            Call CloseSqlServerConection_Lib
            Exit Sub
        End If
        
        Set rnTargetCell = Sheets(sSheetName).Range(sTargetCell)
        rnTargetCell.Offset(1, 0).CopyFromRecordset rs
    
    Call CloseSqlServerConection_Lib
    
    '----------------------------------------------------------------------------------------------------------------
    'Get local to print header
    
    sTargetCellR1C1 = Application.ConvertFormula(Formula:=sTargetCell, FromReferenceStyle:=xlA1, ToReferenceStyle:=xlR1C1)
    lLn = GetRowFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    iCol = GetColumnFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    
    '----------------------------------------------------------------------------------------------------------------
    'Insert Header
    
    Call OpenSqlServerConection_Lib
    
        rs.Open sSqlQuery, conn
        
        For i = 0 To rs.Fields.Count - 1
            Sheets(sSheetName).Cells(lLn, iCol + i).Value = rs.Fields(i).Name
        Next
        
    Call CloseSqlServerConection_Lib
    
    '----------------------------------------------------------------------------------------------------------------
    
    Set rn = rnTargetCell.CurrentRegion
    Set rnHeader = GetHeader_Lib(sExecutionID, sUser, rn)
    Set rnWithoutHeader = GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn)
    Call FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub


Function GetRate_Lib(companyRecebido) As Double

Stop
'Ajustar Function para recebder dinamicamente o local de destino e n�o est�tico como est� programado
'Criar procedure para substituir select comum
'Atualizar nomes das vari�veis sql, rs, cn para padr�es com o tipo da vari�vel no sufixo do nome da mesma

Call SetSheetsVariables

Call OpenSqlExcelConection
SQL = "Select [InvertedRate] From Rates Where [Currency] = 'USD'"
Set rs = cn.Execute(SQL)
wsTemp.[z2].CopyFromRecordset rs
Call CloseSqlExcelConection
    
End Function


Sub LoadSPL086000030_Lib(sExecutionID, sUser, IdReconciliationCover, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub LoadSPL086000030_Lib(sExecutionID, sUser, IdReconciliationCover, sSheetName, sRangeName, sTargetCell)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & IdReconciliationCover & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_GetSPL086000030 @Year = " & iYear & ", @Month =" & iMonth & ", @IdReconciliationCover = " & "'" & IdReconciliationCover & "'"
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetFailedTestDetails_Lib(sExecutionID, sUser)

Stop
'Verificar se esta fun��o ainda � �til
'Esta fun��o retorna uma �nica string com todos os detalhes do erro concatenados.

sSqlQuery = "Execute [SP_GetFailedTestDetails] @ExecutionID ='" & sExecutionID & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)

If rs.EOF = False Then
    GetFailedTestDetails_Lib = rs.Fields(0)
End If
    
Call CloseSqlServerConection_Lib

End Function

Function GetAleatoryGuid_Lib()

    'Substituir todas as implementa��es desta fun��o pela fun��o CreateGuidString_Lib() presente no Mod_Guid
    Call OpenSqlServerConection_Lib

    Set rs = conn.Execute("SP_GetAleatoryGuid")

    If rs.EOF = False Then
        GetAleatoryGuid_Lib = rs.Fields(0)
        GetAleatoryGuid_Lib = replace(GetAleatoryGuid_Lib, "{", "")
        GetAleatoryGuid_Lib = replace(GetAleatoryGuid_Lib, "}", "")
        'Debug.Print GetAleatoryGuid_Lib & String(5, " ") & " Len: " & Len(GetAleatoryGuid_Lib)
        Debug.Print "Substituir dodas as implementa��es da Function GetAleatoryGuid_Lib() pela Function CreateGuidString_Lib()"
    End If
    
    rs.Close
    Set rs = Nothing
    
    CloseSqlServerConection_Lib

End Function

Function GetDatetimeInsert_Lib(sExecutionID, sUser) As String

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetDatetimeInsert_Lib(sExecutionID, sUser) As String"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Call OpenSqlServerConection_Lib

        Set rs = conn.Execute("Execute SP_GetDatetimeInsert")
    
        If rs.EOF = False Then
            GetDatetimeInsert_Lib = rs.Fields(0)
        End If
    
    CloseSqlServerConection_Lib
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function GetExecutionAutomationSOParameters_Lib(sExecutionID, sUser, SOParameter) As String

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String
Dim sQuery                      As String

sFunctionName = "Function GetExecutionAutomationSOParameters_Lib(sExecutionID, sUser, SOParameter) As String"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & SOParameter
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Call OpenSqlServerConection_Lib
    sQuery = "Execute [SP_GetExecutionAutomationSOParameterValue] @ExecutionID = '" & sExecutionID & "', @Parameter = '" & SOParameter & "'"
        
    Set rs = conn.Execute(sQuery)

    If rs.EOF = False Then
        GetExecutionAutomationSOParameters_Lib = rs.Fields(0)
        'GetExecutionAutomationSOParameters_Lib = replace(GetAleatoryGuid_Lib, "{", "")
        'GetExecutionAutomationSOParameters_Lib = replace(GetAleatoryGuid_Lib, "}", "")
    End If
    
    rs.Close
    Set rs = Nothing
    
    CloseSqlServerConection_Lib
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Sub GetRateReconciliationCover_Lib(sExecutionID, sUser, iYear, iMonth, sSheetName, sRangeName, sTargetCell)

'Como Chamar a Fun��o
'sSheetName = "Temp"
'sRangeName "Rate"
'sTargetCell = "A1"

Dim sR1C1StartRange             As String
Dim sR1C1EndRange               As String
Dim rnStartRange                As Range
Dim rnEndRange                  As Range
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub GetRateReconciliationCover_Lib(sExecutionID, sUser, iYear, iMonth, sSheetName, sRangeName, sTargetCell)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & iYear & ", " & iMonth & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_GetRateReconciliationCover @Year = " & iYear & ", @Month =" & iMonth
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)
    
    Sheets(sSheetName).Select
    
    sTargetCellR1C1 = Application.ConvertFormula(Formula:=sTargetCell, FromReferenceStyle:=xlA1, ToReferenceStyle:=xlR1C1)
    lLn = GetRowFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    iCol = GetColumnFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1)
    
    Set rn = Cells(lLn, iCol)
    
    sR1C1StartRange = ConvertRangeToR1C1_Lib(rn.Offset(1, 3))
    sR1C1EndRange = ConvertRangeToR1C1_Lib(rn.Offset(2, 3))
    
    Set rnStartRange = rn.Offset(1, 2)
    Set rnEndRange = rn.Offset(2, 2)
    Set rnRange = Range(rnStartRange, rnEndRange)
    
    For Each rn In rnRange
        
        rn.Select
    
        Select Case rn.Value
            Case "ARS"
                ActiveWorkbook.Names.Add Name:="ARS", RefersToR1C1:="=Temp!" & sR1C1StartRange
            Case "USD"
                ActiveWorkbook.Names.Add Name:="USD", RefersToR1C1:="=Temp!" & sR1C1EndRange
            Case Else
                Stop
                'Escrever Teste
                'Retirar Msgbox
                'MsgBox "There are currencies not registered in the Function GetRateReconciliationCover_Lib at the library General Functions.xlam." & Chr(13) & "Check all currency in that code and complete de missing currency.", vbInformation, "WARNING"
        End Select
    
    Next rn

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub GetRateDetail_Lib(sExecutionID, sUser, iYear, iMonth, bolIsInAging, sAgingType, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub GetRateDetail_Lib(sExecutionID, sUser, iYear, iMonth, bolIsInAging, sAgingType, sSheetName, sRangeName, sTargetCell)"
sFunctionParameters = sExecutionID & ", " & sUser & "," & iYear & "," & iMonth & "," & bolIsInAging & "," & sAgingType & "," & sSheetName & "," & sRangeName & "," & sTargetCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_GetRateDetail @Year = " & iYear & ", @Month = " & iMonth & ", @Type = " & "'" & sAgingType & "'" & ", @IsInAging = " & bolIsInAging
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

'Sub GetOSParametersList_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)
    
'Dim sFunctionName            As String
'Dim sFunctionParameters      As String
'Dim sExecutionExitMode       As String

'sFunctionName = "Sub GetOSParametersList_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)"
'sFunctionParameters = sExecutionID & "," & sSheetName & "," & sRangeName & "," & sTargetCell
'Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

'    sSqlQuery = "Execute [SP_GetSORegionalSettingsList]"
'    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

'sExecutionExitMode = "Traditional"
'Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

'End Sub

Sub LoadBusinessPlace_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

    sFunctionName = "Sub LoadBusinessPlace_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)"
    sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sSqlQuery = "Execute SP_GetBusinessPlace"
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub GetReconciliationCoverAccounts_Lib(sExecutionID, sUser, sIdReconciliationCover, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

    sFunctionName = "Sub GetReconciliationCoverAccounts_Lib(sExecutionID, sUser, sIdReconciliationCover, sSheetName, sRangeName, sTargetCell)"
    sFunctionParameters = sExecutionID & ", " & sUser & ", " & sIdReconciliationCover & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_GetReconciliationCoverAccounts @IdReconciliationCover ='" & sIdReconciliationCover & "'"
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub GetSegments_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

    sFunctionName = "Sub GetSegments_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)"
    sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Select *, [ID] from T_Segment Order By [Segment]"
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub GetMovingType_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

    sFunctionName = "Sub GetMovingType_Lib(sExecutionID, sUser, sSheetName, sRangeName, sTargetCell)"
    sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSheetName & ", " & sRangeName & ", " & sTargetCell
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Select *, [ID] from T_MovingType Order By [MVt]"
    Call LoadSqlServerData_Lib(sExecutionID, sUser, sSqlQuery, sSheetName, sTargetCell, sRangeName)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetAutomationName_Lib(sExecutionID, sUser) As String

sSqlQuery = "Select [Name] From T_Automations Where [ID] =" & "'" & sExecutionID & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)

    If rs.EOF = False Then
        GetAutomationName_Lib = rs.Fields(0)
    End If
    
    rs.Close
    Set rs = Nothing

Call CloseSqlServerConection_Lib

End Function

Function GetAutomationGitRepository_Lib(sAutomationID, sUser) As String

sSqlQuery = "Select [GitRepository] From T_Automations Where [ID] =" & "'" & sAutomationID & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)

    If rs.EOF = False Then
        GetAutomationGitRepository_Lib = rs.Fields(0)
    End If
    
    rs.Close
    Set rs = Nothing

Call CloseSqlServerConection_Lib


End Function

Function GetAutomationMainFileName_Lib(sAutomationID, sUser) As String

sSqlQuery = "Select [MainAutomationFile] From T_Automations Where [ID] =" & "'" & sAutomationID & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)

    If rs.EOF = False Then
        GetAutomationMainFileName_Lib = rs.Fields(0)
    End If
    
    rs.Close
    Set rs = Nothing

Call CloseSqlServerConection_Lib


End Function

Function GetAllExecutionLogByID_Lib(sSqlQuery)

Dim iCols
Dim iCountOfColumns As Integer

Call OpenSqlServerConection_Lib

Set rs = conn.Execute(sSqlQuery)
rs.MoveFirst
GetAllExecutionLogByID_Lib = rs.GetRows

Call CloseSqlServerConection_Lib

End Function

Function GetOSParametersList_Lib(sExecutionID, sUser) As Variant

Dim i                               As Integer
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Function GetOSParametersList_Lib(sExecutionID, sUser) As Variant"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
       
    sSqlQuery = "Execute [SP_GetSORegionalSettingsList]"
    
    Call OpenSqlServerConection_Lib
        Set rs = conn.Execute(sSqlQuery)
        rs.MoveFirst
        GetOSParametersList_Lib = rs.GetRows
    Call CloseSqlServerConection_Lib
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function GetQueryHeader_Lib(sExecutionID, sUser, sSqlQueryToGetHeader, iCountOfColumns)

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Function GetQueryHeader_Lib(sExecutionID, sUser, sSqlQueryToGetHeader, iCountOfColumns)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSqlQueryToGetHeader & ", " & iCountOfColumns
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    ReDim aHeader(iCountOfColumns)
    
    Call OpenSqlServerConection_Lib
    Set rs = conn.Execute(sSqlQueryToGetHeader)
    
    For i = 0 To rs.Fields.Count - 1
        aHeader(i) = rs.Fields(i).Name
    Next
    
    GetQueryHeader_Lib = aHeader
    
    'rs.MoveFirst
    'GetQueryHeader_Lib = rs.GetRows
    
    Call CloseSqlServerConection_Lib

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function
