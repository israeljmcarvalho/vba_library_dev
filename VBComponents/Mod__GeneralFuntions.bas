Attribute VB_Name = "Mod__GeneralFuntions"
Option Explicit

Sub AutoFitEntireColumn_Lib(sExecutionID, sUser, aSheetsArray)

Dim i                                   As Integer
Dim iArraySize                          As Integer
Dim sSheetName                          As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub AutoFitEntireColumn_Lib(sExecutionID, sUser, aSheetsArray)"
sFunctionParameters = sExecutionID & ", " & sUser & ", ARRAY aAccountReceivable, " & iDefaultArraySize
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    For i = 0 To iArraySize
    
        sSheetName = aSheetsArray(i, 0)
        Sheets(sSheetName).Select
        Cells.EntireColumn.AutoFit
        [B2].Select
        ActiveWindow.FreezePanes = True
        
    Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetAutomationMode_Lib(sExecutionID, sUser) As String

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetAutomationMode_Lib(sExecutionID, sUser) As String"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    If Left(Application.ActiveWorkbook.Path, 18) = "C:\Users\icarvalho" Then
        GetAutomationMode_Lib = "Dev"
    Else
        GetAutomationMode_Lib = "Prod"
    End If
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
    
End Function

Function GetRootPath_Lib(sExecutionID, sUser) As String

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String
Dim sAutomationMode                     As String
Dim sUserLcase                          As String

sFunctionName = "Function GetRootPath_Lib(sExecutionID, sUser) As String"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sAutomationMode = GetAutomationMode_Lib(sExecutionID, sUser)
    sUserLcase = LCase(sUser)
    
    If sAutomationMode = "Dev" Then
        GetRootPath_Lib = "C:\Users\" & sUserLcase & cDevDefaultPath
    ElseIf sAutomationMode = "Prod" Then
        GetRootPath_Lib = cProdDefaultPath
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function GetFileDateTime_Lib(sExecutionID, sUser, sFilePath) As String

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetFileDateTime_Lib(sExecutionID, sUser, sFilePath) As String"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sFilePath
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    GetFileDateTime_Lib = FileDateTime(sFilePath)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Sub ClearContents_Lib(sExecutionID, sUser, rnRange)

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub ClearContents_Lib(sExecutionID, sUser, rnRange)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rnRange.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    For Each rn In rnRange
    
    If rn.Font.ColorIndex = 3 Then
        rn.Clear
    End If
    
    Next rn
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub GetPeriod_Lib(sExecutionID, sUser, sSheetName, sTargetCell, sXlDateOrder)

Dim rnTargetCell                        As Range
Dim sR1C1PeriodAddress                  As String
Dim sR1C1YearAddress                    As String
Dim sR1C1MonthAddress                   As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub GetPeriod_Lib(sExecutionID, sUser, sSheetName, sTargetCell, sXlDateOrder)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSheetName & ", " & sTargetCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    'Retorna os valores dtPeriod, iYear e iMonth para a sheet passada como par�metro na chamada da fun��o
    Set rnTargetCell = Sheets(sSheetName).Range(sTargetCell)
    
    'VarType
    'Debug.Print "rnTargetCell =>> VarType is " & UCase(TypeName(rnTargetCell))

    rnTargetCell.Value = "Period"
    rnTargetCell.Offset(0, 1).Value = "Year"
    rnTargetCell.Offset(0, 2).Value = "Month"
    
    'Stop
    
    Call SetPeriod_Lib(sExecutionID, sUser, sXlDateOrder)
    
    'Set rnRange = ConvertStringToR1C1_Lib(sExecutionID, sUser, sTargetCell)
       
    'sR1C1PeriodAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnRange.Offset(1, 0))
    'sR1C1YearAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnRange.Offset(1, 1))
    'sR1C1MonthAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnRange.Offset(1, 2))
    
    sR1C1PeriodAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnTargetCell.Offset(1, 0))
    sR1C1YearAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnTargetCell.Offset(1, 1))
    sR1C1MonthAddress = "=" & sSheetName & "!" & ConvertRangeToR1C1_Lib(rnTargetCell.Offset(1, 2))
    
    'Debug.Print sR1C1PeriodAddress
    'Debug.Print sR1C1YearAddress
    'Debug.Print sR1C1MonthAddress
    
    rnTargetCell.Offset(1, 0).Value = dtPeriod
    rnTargetCell.Offset(1, 1).Value = iYear
    rnTargetCell.Offset(1, 2).Value = iMonth
    
    ActiveWorkbook.Names.Add Name:="dtPeriod", RefersToR1C1:=sR1C1PeriodAddress
    ActiveWorkbook.Names.Add Name:="iYear", RefersToR1C1:=sR1C1YearAddress
    ActiveWorkbook.Names.Add Name:="iMonth", RefersToR1C1:=sR1C1MonthAddress
  
    Set rn = rnTargetCell.CurrentRegion
    Set rnHeader = GetHeader_Lib(sExecutionID, sUser, rn)
    Set rnWithoutHeader = GetRangeWithoutHeader_Lib(sExecutionID, sUser, rn)
    Call FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub SetPeriod_Lib(sExecutionID, sUser, sXlDateOrder)

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String
Dim sLastDay                            As String

sFunctionName = "Sub SetPeriod_Lib(sExecutionID, sUser, sXlDateOrder)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sXlDateOrder
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    'Define valores para as vari�veis dtPeriod, iYear e iMonth com base no �ltimo dia do m�s anterior
    
    If sXlDateOrder = "0" Then 'Order of date elements:�0 = month-day-year
        dtPeriod = CDate(Month(Date) & "/" & 1 & "/" & Year(Date)) - 1
    ElseIf sXlDateOrder = "1" Then 'Order of date elements:�1 = day-month-year
        dtPeriod = CDate(1 & "/" & Month(Date) & "/" & Year(Date)) - 1
    ElseIf sXlDateOrder = "2" Then 'Order of date elements:�2 = year-month-day
        dtPeriod = CDate(1 & "/" & Month(Date) & "/" & Year(Date)) - 1
    End If
    
    iYear = Year(dtPeriod)
    iMonth = Month(dtPeriod)
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub


Function GetCountOfRecords_Lib(sExecutionID, sUser, rn) As Long

Dim sR1C1                               As String
Dim sR1C1SecodPart                      As String
Dim sR1C1CountRows                      As String
Dim iSecondString_C_Position            As Integer
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetCountOfRecords_Lib(sExecutionID, sUser, rn) As Long"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sR1C1 = CStr(rn.Address(ReferenceStyle:=xlR1C1))
    sR1C1SecodPart = Mid(sR1C1, InStr(sR1C1, ":") + 2)
    iSecondString_C_Position = InStr(sR1C1SecodPart, "C")
    GetCountOfRecords_Lib = CLng(Left(sR1C1SecodPart, iSecondString_C_Position - 1))
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


Sub GetColumnIndex_Lib(sExecutionID, sUser, rn)

Dim sR1C1                               As String
Dim sR1C1SecodPart                      As String
Dim sR1C1CountRows                      As String
Dim iSecondString_C_Position            As Integer
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub GetColumnIndex_Lib(sExecutionID, sUser, rn)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sR1C1 = CStr(rn.Address(ReferenceStyle:=xlR1C1))
    Stop
    'Precisa mesmo do Debug.Print Abaixo?
    Debug.Print "R1C1 = " & sR1C1
    
    R1C1SecodPart = Mid(sR1C1, InStr(sR1C1, "C") + 1)
    Stop
    'Precisa mesmo do Debug.Print Abaixo?
    Debug.Print "sR1C1SecodPart = " & sR1C1SecodPart
    
    SecondString_C_Position = InStr(sR1C1SecodPart, "C")
    GetColumnIndex = CInt(Left(sR1C1SecodPart, iSecondString_C_Position - 1))

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub ClearAllNames_Lib(sExecutionID, sUser, wb)

Dim MyName                              As Name
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub ClearAllNames_Lib(sExecutionID, sUser, wb)"
sFunctionParameters = sExecutionID & ", " & sUser & "WORKBOOK " & wb.Name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    On Error Resume Next
    Application.DisplayAlerts = False
    
    For Each MyName In Names
        wb.Names(MyName.Name).Delete
    Next
    
    Application.DisplayAlerts = True
    On Error GoTo 0

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub SetFreezePanes_Lib(sExecutionID, sUser, wbMain, rn)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub SetFreezePanes_Lib(sExecutionID, sUser, wbMain, rn)"
sFunctionParameters = sExecutionID & ", " & sUser & ", WORKBOOK " & wbMain.Name & ", RANGE " & rn.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Application.ScreenUpdating = True
    wbMain.Activate
    rn.Select
    On Error Resume Next
        ActiveWindow.FreezePanes = True
    On Error GoTo 0

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetRowFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1) As Long

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Function GetRowFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1) As Long"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sTargetCellR1C1
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    GetRowFromStringRange_Lib = replace(Left(sTargetCellR1C1, InStr(sTargetCellR1C1, "C") - 1), "R", "")

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function GetColumnFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1) As Integer

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Function GetColumnFromStringRange_Lib(sExecutionID, sUser, sTargetCellR1C1) as Integer"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sTargetCellR1C1
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    GetColumnFromStringRange_Lib = (Mid(sTargetCellR1C1, InStr(sTargetCellR1C1, "C") + 1))
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Function MoveSheets_Lib_Anterior(sOrigemSheet, sTargetSheet)

Stop

If sTargetSheet = "0" Then
Stop
    Sheets(sOrigemSheet).Move Before:=Sheets(5)
Else
    Sheets(sOrigemSheet).Move After:=sTargetSheet
End If
    
End Function

Sub AddTableTotal_Lib(sExecutionID, sUser, rnTargetCell)

Dim rnLastRow                   As Range
Dim sColumnLetter               As String
Dim iColumnIndex                As Integer
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub AddTableTotal_Lib(sExecutionID, sUser, rnTargetCell)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rnTargetCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    iColumnIndex = rnTargetCell.Column
    Set rnLastRow = rnTargetCell.Offset(-1, 0)
    sColumnLetter = GetColumnLetter_Lib(iColumnIndex)
    rnTargetCell.Formula = "=SUM(" & sColumnLetter & "2:" & sColumnLetter & rnLastRow.Row & ")"
    Calculate
    rnTargetCell.Font.Bold = True
    Cells.EntireColumn.AutoFit

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetColumnLetter_Lib(iColumnIndex As Integer) As String

Dim vArr
    
vArr = Split(Cells(1, iColumnIndex).Address(True, False), "$")
GetColumnLetter_Lib = vArr(0)

End Function

Sub Duration_Lib(sExecutionID, sUser, dtNow)

Dim sDuration As Date
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub Duration_Lib(sExecutionID, sUser, dtNow)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & dtNow
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sDuration = Now() - dtNow
    MsgBox Minute(sDuration) & " minutes " & Second(sDuration) & " seconds", vbInformation, "D U R A T I O N"

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
    
End Sub

Sub ConvertColumnsToString_Lib(sExecutionID, sUser, iStartLine, iForColumnNumber, aColumnToString)

Dim iCol                        As Integer
Dim lLn                         As Integer
Dim iArraySize                  As Integer
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub ConvertColumnsToString_Lib(sExecutionID, sUser, iStartLine, iForColumnNumber, aColumnToString)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & iStartLine & ", " & iForColumnNumber & ", ARRAY aColumnToString"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    iArraySize = UBound(aColumnToString)
    
    For iCol = 0 To iArraySize
       'Columns(CInt(aColumnToString(iCol))).Interior.ColorIndex = 6
        Columns(CInt(aColumnToString(iCol))).NumberFormat = "@"
    Next
    
    For lLn = lStartLine To Cells(200000, iForColumnNumber).End(xlUp).Row
        For iCol = 0 To iArraySize
            'Cells(lLn, CInt(aColumnToString(iCol))).Select
            If Not IsEmpty(Cells(lLn, CInt(aColumnToString(iCol)))) Then
                'Cells(lLn, CInt(aColumnToString(iCol))).Select
                'Cells(lLn, CInt(aColumnToString(iCol))).Interior.ColorIndex = 7
                Cells(lLn, CInt(aColumnToString(iCol))).Value = CStr(Cells(lLn, CInt(aColumnToString(iCol))))
            End If
        Next
    Next


sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function GetRunMode_Lib(sUser)

Stop

If sUser = "ICARVALHO" Then
    GetRunMode_Lib = "Dev"
Else
    GetRunMode_Lib = "Prod"
End If

End Function

Function PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, Optional sFunctionParameters As String = "NULL")
'Function PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, Optional sFunctionParameters)

Dim sMoment                 As String
Dim sExecutionExitMode      As String

sFunctionParameters = replace(sFunctionParameters, "'", "")
sMoment = "IN"
sExecutionExitMode = "-"
Call SendExecutionLog_Lib(sAutomationID, sExecutionID, sMoment, sFunctionName, sFunctionParameters, sExecutionExitMode, sUser)
 
End Function

Function PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, Optional sFunctionParameters)

Dim sMoment          As String

sFunctionParameters = replace(sFunctionParameters, "'", "")

sMoment = "OUT"
Call SendExecutionLog_Lib(sAutomationID, sExecutionID, sMoment, sFunctionName, sFunctionParameters, sExecutionExitMode, sUser)

End Function

Sub DeleteNamedRanges_Lib(sExecutionID, sUser, sWorkbook)

Dim MyName                              As Name
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub DeleteNamedRanges_Lib(sExecutionID, sUser, sWorkbook)"
sFunctionParameters = sExecutionID & ", " & sUser & ", WORKBOOK" & sWorkbook
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Windows(sWorkbook).Activate
    
    For Each MyName In Names
    
        'Debug.Print ActiveWorkbook.Names(MyName.Name)
        
        On Error Resume Next
            ActiveWorkbook.Names(MyName.Name).Delete
        On Error GoTo 0
    Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub BreakExternalLinks_Lib(sExecutionID, sUser, wb)


Dim ExternalLinks                       As Variant
Dim x                                   As Long
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub BreakExternalLinks_Lib(sExecutionID, sUser, wb)"
sFunctionParameters = sExecutionID & ", " & sUser & ", WORKBOOK" & wb.Name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    On Error Resume Next
    'Create an Array of all External Links stored in Workbook
      ExternalLinks = wb.LinkSources(Type:=xlLinkTypeExcelLinks)
    
    'Loop Through each External Link in ActiveWorkbook and Break it
      For x = 1 To UBound(ExternalLinks)
        wb.BreakLink Name:=ExternalLinks(x), Type:=xlLinkTypeExcelLinks
      Next x
      
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub PrintExecutionID_Lib(sExecutionID, sUser, sAutomationID, sAutomationName, sAutomationMainFileName)

    Debug.Print Chr(13) & String(66, "=") & _
    Chr(13) & _
    "AutomationName: " & sAutomationName & _
    Chr(13) & _
    "sAutomationMainFileName: " & sAutomationMainFileName & _
    Chr(13) & _
    "AutomationID: " & sAutomationID & _
    Chr(13) & _
    "ExecutionID Generated by VBA: " & sExecutionID
    
End Sub

Function GetVarType_Lib(Variable) As String

Dim sConstant           As String
Dim sValue              As String
Dim sDescription        As String

MsgBox (Variable)
Select Case VarType(Variable)

    Case Is = 0
        sConstant = "vbEmpty"
        sDescription = "(uninitialized)"
    Case Is = 1
        sConstant = "vbNull"
        sDescription = "(no valid data))"
    Case Is = 2
        sConstant = "vbInteger"
        sDescription = "Integer"
    Case Is = 3
        sConstant = "vbLong"
        sDescription = "Long integer"
    Case Is = 4
        sConstant = "vbSingle"
        sDescription = "Single-precision floating-point number"
    Case Is = 5
        sConstant = "vbDouble"
        sDescription = "Double-precision floating-point number"
    Case Is = 6
        sConstant = "vbCurrency"
        sDescription = "Currency Value"
    Case Is = 7
        sConstant = "vbDate"
        sDescription = "Date "
    Case Is = 8
        sConstant = "vbString"
        sDescription = "String"
    Case Is = 9
        sConstant = "vbObject"
        sDescription = "Object "
    Case Is = 10
        sConstant = "vbError"
        sDescription = "Error value"
    Case Is = 11
        sConstant = "vbBoolean"
        sDescription = "Boolean value"
    Case Is = 12
        sConstant = "vbVariant"
        sDescription = "Variant (used only with arrays of variants)"
    Case Is = 13
        sConstant = "vbDataObject"
        sDescription = "A Data Access Object"
    Case Is = 14
        sConstant = "vbDecimal"
        sDescription = "Decimal value"
    Case Is = 17
        sConstant = "vbByte"
        sDescription = "Byte Value"
    Case Is = 20
        sConstant = "vbLongLong"
        sDescription = "LongLong integer (valid on 64-bit platforms only)"
    Case Is = 36
        sConstant = "vbUserDefinedType"
        sDescription = "Variants that contain user-defined types"
    Case Is = 8192
        sConstant = "vbArray"
        sDescription = "Array (always added to another constant when returned by this function)"
End Select

GetVarType_Lib = "Variable: " & Variable & " | Type: " & sConstant & " | VariableTypeDescription: " & sDescription

End Function


Function GetVariableTypeName_Lib(Variable) As String

GetVariableTypeName_Lib = TypeName(Variable)

End Function


Function GetLastCharPosition_Lib(sString, sChar, Optional sExecutionID, Optional sUser) As Integer

Dim iLen                                As Integer
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String


If Not IsMissing(sExecutionID) And Not IsMissing(sUser) Then
    sFunctionName = "Function GetLastCharPosition_Lib(sString, sChar, Optional sExecutionID, Optional sUser) As Integer"
    sFunctionParameters = sString & ", " & sExecutionID & ", " & sChar
    Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
End If
    
iLen = Len(sString)

For i = iLen To 1 Step -1
    If Mid(sString, i - 1, 1) = sChar Then
        GetLastCharPosition_Lib = i
        Exit For
    End If
Next i

If Not IsMissing(sExecutionID) And Not IsMissing(sUser) Then
    sExecutionExitMode = "Traditional"
    Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
End If

End Function

Function GetHostName_Lib(sExecutionID, sUser) As String


Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Function GetHostName_Lib(sExecutionID, sUser) As String"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    GetHostName_Lib = Environ$("computername")
      
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function

Sub GetOSParameters_Lib(sExecutionID, sUser, aSheetsArray)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub SetF19Header(sExecutionID)"
sFunctionParameters = sExecutionID & ", " & sUser & " ARRAY aSheetsArray"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

'https://docs.microsoft.com/en-us/office/vba/api/excel.application.international

    sXlLeftBrace = Application.International(xlLeftBrace)
    sXlLeftBracket = Application.International(xlLeftBracket)
    sXlLowerCaseColumnLetter = Application.International(xlLowerCaseColumnLetter)
    sXlLowerCaseRowLetter = Application.International(xlLowerCaseRowLetter)
    sXlRightBrace = Application.International(xlRightBrace)
    sXlRightBracket = Application.International(xlRightBracket)
    sXlUpperCaseColumnLetter = Application.International(xlUpperCaseColumnLetter)
    sXlUpperCaseRowLetter = Application.International(xlUpperCaseRowLetter)
    lXlCountryCode = Application.International(xlCountryCode)
    lXlCountrySetting = Application.International(xlCountrySetting)
    sXlGeneralFormatName = Application.International(xlGeneralFormatName)
    bolXlCurrencyBefore = Application.International(xlCurrencyBefore)
    sXlCurrencyCode = Application.International(xlCurrencyCode)
    lXlCurrencyDigits = Application.International(xlCurrencyDigits)
    bolXlCurrencyLeadingZeros = Application.International(xlCurrencyLeadingZeros)
    bolXlCurrencyMinusSign = Application.International(xlCurrencyMinusSign)
    lXlCurrencyNegative = Application.International(xlCurrencyNegative)
    bolXlCurrencySpaceBefore = Application.International(xlCurrencySpaceBefore)
    bolXlCurrencyTrailingZeros = Application.International(xlCurrencyTrailingZeros)
    lXlNoncurrencyDigits = Application.International(xlNoncurrencyDigits)
    bolXl24HourClock = Application.International(xl24HourClock)
    bolXl4DigitYears = Application.International(xl4DigitYears)
    lXlDateOrder = Application.International(xlDateOrder)
    sXlDateSeparator = Application.International(xlDateSeparator)
    sXlDayCode = Application.International(xlDayCode)
    bolXlDayLeadingZero = Application.International(xlDayLeadingZero)
    sXlHourCode = Application.International(xlHourCode)
    bolXlMDY = Application.International(xlMDY)
    sXlMinuteCode = Application.International(xlMinuteCode)
    sXlMonthCode = Application.International(xlMonthCode)
    bolXlMonthLeadingZero = Application.International(xlMonthLeadingZero)
    lXlMonthNameChars = Application.International(xlMonthNameChars)
    sXlSecondCode = Application.International(xlSecondCode)
    sXlTimeSeparator = Application.International(xlTimeSeparator)
    bolXlTimeLeadingZero = Application.International(xlTimeLeadingZero)
    lXlWeekdayNameChars = Application.International(xlWeekdayNameChars)
    sXlYearCode = Application.International(xlYearCode)
    bolXlMetric = Application.International(xlMetric)
    bolXlNonEnglishFunctions = Application.International(xlNonEnglishFunctions)
    sXlAlternateArraySeparator = Application.International(xlAlternateArraySeparator)
    sXlColumnSeparator = Application.International(xlColumnSeparator)
    sXlDecimalSeparator = Application.International(xlDecimalSeparator)
    sXlListSeparator = Application.International(xlListSeparator)
    sXlRowSeparator = Application.International(xlRowSeparator)
    sXlThousandsSeparator = Application.International(xlThousandsSeparator)

Stop

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function SetExecutionLog_Lib(sAutomationID, sExecutionID, sUser)

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sSqlQuery = "Execute [SP_SetExecutionLog]" & _
     "@AutomationId = '" & sAutomationID & "', " & _
     "@ExecutionID = '" & sExecutionID & "', " & _
     "@User = '" & sUser & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)
Call CloseSqlServerConection_Lib


End Function


Sub PrintTestResult_Lib(sExecutionID, sUser)

Dim aFailedTestDetail
Dim aQueryHeader
Dim sSqlQueryToGetHeader            As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub PrintTestResult_Lib(sExecutionID, sUser)"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    sSqlQuery = "Execute SP_GetAllExecutionLogByID @ExecutionID = '" & sExecutionID & "'"
    aFailedTestDetail = GetAllExecutionLogByID_Lib(sSqlQuery)
      
    'N�o ser� printado os dois primeiros campos (index 0 e 1) pois estas informa��es (AutomationName e ExecutinoID) ja foram
    'printadas na Immediate Window do VBE no in�cio da execu��o da automa��o
    For i = 2 To UBound(aFailedTestDetail)
        aFailedTestDetail(i, 0) = replace(aFailedTestDetail(i, 0), "{", "")
        aFailedTestDetail(i, 0) = replace(aFailedTestDetail(i, 0), "}", "")
    Next
    
    sSqlQueryToGetHeader = "Execute SP_GetAllExecutionLogByID @ExecutionID = '" & sExecutionID & "'"
    iCountOfColumns = UBound(aFailedTestDetail)
    aQueryHeader = GetQueryHeader_Lib(sExecutionID, sUser, sSqlQueryToGetHeader, iCountOfColumns)
    
    iCountOfRecords = 1
    
    ReDim aMatriz(iCountOfColumns, iCountOfColumns)
    
    For i = 2 To iCountOfColumns
        aMatriz(i, i) = aQueryHeader(i) & ": " & aFailedTestDetail(i, 0)
        Debug.Print aMatriz(i, i)
    Next
    
    Debug.Print String(66, "=") & Chr(13)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

