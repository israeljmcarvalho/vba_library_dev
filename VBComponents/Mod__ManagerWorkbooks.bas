Attribute VB_Name = "Mod__ManagerWorkbooks"
Option Explicit

Sub CloseWorkbook_Lib(sExecutionID, sUser, sFileName, sFileToReturn, wsSheetToReturn, bolSaveChanges)

Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub CloseWorkbook_Lib(sExecutionID, sUser, sFileName, sFileToReturn, wsSheetToReturn, bolSaveChanges)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sFileName & ", " & sFileToReturn & "SHEET " & wsSheetToReturn.Name & ", " & bolSaveChanges
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Windows(sFileName).Activate
    Application.DisplayAlerts = False
    ActiveWorkbook.Close SaveChanges:=bolSaveChanges
    Application.DisplayAlerts = True
    Windows(sFileToReturn).Activate
    wsSheetToReturn.Select
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

