Attribute VB_Name = "Mod__PublicVariables"
'Public Const cDevDefaultPath             As String = "\OneDrive - Lear Corporation\VBA\Git\"
Public Const cDevDefaultPath             As String = "\Documents\VBA\Git\"
Public Const cProdDefaultPath            As String = "\\brscjoi-fp01\groups\SSC\SSC AUTOMATION\VBA\Git\"
Public Const cOriginalFilesFolder        As String = "Original Files"
Public Const cOutputFilesFolder          As String = "Output"
Public Const cVBComponentsFolder         As String = "VBComponents"

Public conn                              As New ADODB.Connection
Public rs                                As New ADODB.Recordset

Public cn                                As Object
Public rs2                               As Object

Public sDefaultPath                      As String

Public sFolder                           As String
Public sPath                             As String
Public sFile                             As String
Public sFileName                         As String
Public sFilePath                         As String
Public sFileDateTime                     As String
Public sUiPathReturn                     As String
Public sAutomationID                     As String
Public sCell                             As String


'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

'ARQUIVOS DE SUPORTE XLSM

'Vari�vel utilizada para armazenar o SUBFOLDER onde estar�o os arquivos XLSM de suporte
'Ex: "\VBA\Concilia��es\Contas a Pagar"
Public sSuportFilesLocation              As String


'Vari�vel utilizada para armazenar o PATH COMPLETO onde estar�o os arquivos XLSM de suporte
'Esta vari�vel ser� abastecida atrav�s da Function abaixo onde ser�o passados como par�metros:
    '- |sUser| para se obter a parte 'inicial do PATH (Se OneDrive M�quina Local ICARVALHO ou se BRSCJOI)
    '- |sSuportFilesLocation| para se obter a parte final do PATH
'sSuportFilesPath = GetOriginalFilesPath(sUser, sSuportFilesLocation)
'Ex: "C:\Users\icarvalho\OneDrive - Lear Corporation\VBA\Concilia��es\Contas a Pagar"
Public sSuportFilesPath                  As String
        
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
'ARQUIVOS ORIGINAIS
        
'Vari�vel utilizada para armazenar o SUBFOLDER onde estar�o os arquivos ORIGINAIS
'Ex: "\VBA\Concilia��es\Contas a Pagar\Arquivos Originais"
Public sOriginalFilesLocation            As String


'Vari�vel utilizada para armazenar o PATH COMPLETO onde estar�o os arquivos ORIGINAIS
'Esta vari�vel ser� abastecida atrav�s da Function abaixo onde ser�o passados como par�metros:
    '- |sUser| para se obter a parte 'inicial do PATH (Se OneDrive M�quina Local ICARVALHO ou se BRSCJOI)
    '- |sOriginalFilesLocation| para se obter a parte final do PATH
'sOriginalFilesPath = GetOriginalFilesPath(sUser, sOriginalFilesLocation)
'Ex: C:\Users\icarvalho\OneDrive - Lear Corporation\VBA\Concilia��es\Contas a Pagar\Arquivos Originais
Public sOriginalFilesPath                As String

'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Public sOutputFilesPath                  As String




Public sSql                              As String
Public sValues                           As String
Public sRegistros                        As String
Public sActive                           As String
Public sAnalyst                          As String
Public sSqlQuery                         As String
Public sSheetName                        As String
Public sTargetCell                       As String
Public sRangeName                        As String
Public sTargetCellR1C1                   As String
'Public sUser                             As String
Public sIdReconciliationCover            As String
Public sFBL1N2Path                       As String
Public sAgingType                        As String
Public sIsInAging                        As String
Public sString                           As String
Public sProcvFileName                    As String
Public sColumnLetter                     As String
Public sOpenMethod                       As String
Public sGuid                             As String
Public sRunMode                          As String
Public sTestErrorDetails                 As String
Public sTestStatus                       As String
Public sVariableType                     As String

Public qdade_Contas                      As Integer
Public iCol                              As Integer
Public iYear                             As Integer
Public iMonth                            As Integer
Public iDefaultArraySize                 As Integer
Public iRecordLine                       As Integer
Public iForColumnNumber                  As Integer
Public iArraySize                        As Integer
Public iColumnIndex                      As Integer
Public iCountOfColumns                   As Integer
Public iCountOfRecords                   As Integer
Public iSearchColumnNumber               As Integer
Public iFunctionPairExecutionID          As Integer

Public dbAccountBalance                  As Double
Public dbSegmentBalance                  As Double
Public dbAllAccountBalance               As Double
Public dbRate                            As Double

Public lLn                               As Long
Public lRow                              As Long
Public i                                 As Long
Public x                                 As Long
Public lStartLine                        As Long


Public bolOcul                           As Boolean
Public bolTemVcFBL1N                     As Boolean
Public bolTemVcFBL5N                     As Boolean
Public bolTemVcZFI016                    As Boolean
Public bolTemVcReavaliacaoCambial        As Boolean
Public bolIsPossibleNullValue            As Boolean
'Public bolTestsOk                        As Boolean

Public rn                                As Range
Public rnRange                           As Range
Public rnTargetCell                      As Range
Public rnWithoutHeader                   As Range
Public rnHeader                          As Range
Public rnLastRow                         As Range

Public proximo_mes
Public proximo_ano

Public dtPeriod                          As Date
Public dtNow                             As Date

Public aSupportfile()                    As Variant
Public vVariableToCheck

Public aArray()                          As String
Public aColumnToString()                 As String
Public aColumnToAccountFormat()          As String

Public ptPivotTable                      As PivotTable

Public ws2                               As Worksheet
Public ws                                As Worksheet
'Public wsTemp                            As Worksheet
Public wsName                            As Worksheet
'Public wsDatabase                        As Worksheet
Public wsDocumentation                   As Worksheet
Public wsUsers                           As Worksheet
Public wsActualSheet                     As Worksheet
Public wsZFI016                          As Worksheet
Public wsZFI016Vendor                    As Worksheet
Public wsZFI016Customer                  As Worksheet
'Public wsFAGLL03                         As Worksheet
Public wsFBL1N                           As Worksheet
Public wsFBL5N                           As Worksheet
Public wsFAGLFCV                         As Worksheet
Public wsResumoAging                     As Worksheet
Public wsSPL086000030                    As Worksheet
Public wsTargetSheet                     As Worksheet
'Public wsSaldoContabil                   As Worksheet
Public wsCapaBrasil                      As Worksheet
Public wsCapaArgentina                   As Worksheet


Public wbMainWorkbook                    As Workbook
Public wb                                As Workbook


'SO Parameters
Public sXlLeftBrace                           As String
Public sXlLeftBracket                         As String
Public sXlLowerCaseColumnLetter               As String
Public sXlLowerCaseRowLetter                  As String
Public sXlRightBrace                          As String
Public sXlRightBracket                        As String
Public sXlUpperCaseColumnLetter               As String
Public sXlUpperCaseRowLetter                  As String
Public lXlCountryCode                         As Long
Public lXlCountrySetting                      As Long
Public sXlGeneralFormatName                   As String
Public bolXlCurrencyBefore                    As Boolean
Public sXlCurrencyCode                        As String
Public lXlCurrencyDigits                      As Long
Public bolXlCurrencyLeadingZeros              As Boolean
Public bolXlCurrencyMinusSign                 As Boolean
Public lXlCurrencyNegative                    As Long
Public bolXlCurrencySpaceBefore               As Boolean
Public bolXlCurrencyTrailingZeros             As Boolean
Public lXlNoncurrencyDigits                   As Long
Public bolXl24HourClock                       As Boolean
Public bolXl4DigitYears                       As Boolean
Public lXlDateOrder                           As Long
Public sXlDateSeparator                       As String
Public sXlDayCode                             As String
Public bolXlDayLeadingZero                    As Boolean
Public sXlHourCode                            As String
Public bolXlMDY                               As Boolean
Public sXlMinuteCode                          As String
Public sXlMonthCode                           As String
Public bolXlMonthLeadingZero                  As Boolean
Public lXlMonthNameChars                      As Long
Public sXlSecondCode                          As String
Public sXlTimeSeparator                       As String
Public bolXlTimeLeadingZero                   As Boolean
Public lXlWeekdayNameChars                    As Long
Public sXlYearCode                            As String
Public bolXlMetric                            As Boolean
Public bolXlNonEnglishFunctions               As Boolean
Public sXlAlternateArraySeparator             As String
Public sXlColumnSeparator                     As String
Public sXlDecimalSeparator                    As String
Public sXlListSeparator                       As String
Public sXlRowSeparator                        As String
Public sXlThousandsSeparator                  As String
