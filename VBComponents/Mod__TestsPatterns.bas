Attribute VB_Name = "Mod__TestsPatterns"
Option Explicit


Sub CheckLength_Lib(sExecutionID, _
                         sUser, _
                         iTestNumber, _
                         sTestDescription, _
                         iArraySize, _
                         iStartTestLine, _
                         iForColumnNumber, _
                         aFieldsLen)

Dim iColIndex                       As Integer
Dim SearchColumnNumber              As Integer
Dim sExpectedValue                  As String
Dim sRealValue                      As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub CheckLength_Lib(sExecutionID, sUser, iTestNumber,sTestDescription, iArraySize, iStartTestLine,iForColumnNumber,  aFieldsLen)"
sFunctionParameters = sExecutionID & ", " & sUser & " ," & iTestNumber & ", " & sTestDescription & ", " & iArraySize & ", " & iStartTestLine & ", " & iForColumnNumber & ", ARRAY aFieldsLen)"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

For lRow = iStartTestLine To Cells(300000, iForColumnNumber).End(xlUp).Row

    For iColIndex = 0 To iArraySize - 1

        iColumnIndex = iColIndex + 1
        sColumnLetter = GetColumnLetter_Lib(iColumnIndex)

        If Len(Cells(lRow, iColIndex + 1)) > aFieldsLen(iColIndex) Then
        
            SearchColumnNumber = iColIndex + 1
            sExpectedValue = aFieldsLen(iColIndex)
            sRealValue = CStr(Len(Cells(lRow, iColIndex + 1)))
        
                Call FailedPatternOfCellsTests_Lib( _
                    sExecutionID, _
                    sUser, _
                    iTestNumber, _
                    sTestDescription, _
                    SearchColumnNumber, _
                    lRow, _
                    sExpectedValue, _
                    sRealValue)
                
                Exit Sub
                
            Stop
            'Escrever aqui Update TestStatus = true
                
        End If
        
    Next

Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub


Function CheckTheNumberOfFiles_Lib(sExecutionID, sUser, iTestNumber, sTestDescription, sPath)

Dim iCountOfFiles                   As Integer
Dim SearchColumnNumber              As Integer
Dim sExpectedValue                  As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Function CheckTheNumberOfFiles_Lib(sExecutionID, sUser, iTestNumber, sTestDescription, sPath)"
sFunctionParameters = sExecutionID & " ," & sUser & " ," & iTestNumber & ", " & sTestDescription & ", " & sPath
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sPath = Dir(sPath)

        iCountOfFiles = 0
    Do While Len(sPath) > 0
        iCountOfFiles = iCountOfFiles + 1
        sPath = Dir
    Loop
    
    CheckTheNumberOfFiles_Lib = CStr(iCountOfFiles)

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Function


Sub PatternSearchValuesInArrayLoopFor_Lib( _
     sExecutionID, _
     sUser, _
     iTestNumber, _
     sTestDescription, _
     iStartTestLine, _
     iForColumnNumber, _
     iSearchColumnNumber, _
     aExpectedValueArray, _
     bolIsPossibleNullValue)

Dim lRow                                As Long
Dim iArraySize                          As Integer
Dim sExpectedValue                      As String
Dim sRealValue                          As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub PatternSearchValuesInArrayLoopFor_Lib(sExecutionID, sUser, iTestNumber, sTestDescription, iStartTestLine, iForColumnNumber, iSearchColumnNumber, aExpectedValueArray, bolIsPossibleNullValue)"
sFunctionParameters = sExecutionID & " ," & sUser & " ," & iTestNumber & ", " & sTestDescription & ", " & iStartTestLine & ", " & iForColumnNumber & ", " & iSearchColumnNumber & ", " & "ARRAY aExpectedValueArray" & ", " & bolIsPossibleNullValue
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

'TEST PATTERN TYPE
    'Search: Values into an Array
    'Loop: FOR
    
'================================================================================================================================================================
'MONTANDO A STRING COM VALORES ESPERADOS (A vari�vel "sExpectedValuesString" ser� utilizada no msgbox se teste falhar informando quais eram os valores esperados)

'If ArraySize = 1 Then 'Array com 1 �nico elemento

    'ExpectedValueString = ExpectedValueArray(i)
    
    'If IsPossibleNullValue = True Then
    '    ExpectedValueString = ExpectedValueString & "NULL"
    'End If
        
'Else 'Array com mais de um elemento

   Application.ScreenUpdating = False
   
        iArraySize = UBound(aExpectedValueArray) - 1
        
        For i = 0 To iArraySize 'Loop For iterando entre cada elemento do Array
        
            If i = 0 Then 'Primeiro posi��o do Array
                'sExpectedValuesString = aExpectedValueArray(i)
                sExpectedValue = aExpectedValueArray(i)
            ElseIf i <> iArraySize Then  'Demais posi��es do Array
                'sExpectedValuesString = sExpectedValuesString & ", " & aExpectedValueArray(i) ' & ", "
                sExpectedValue = sExpectedValue & ", " & aExpectedValueArray(i) ' & ", "
            ElseIf i = iArraySize Then  '�ltima posi��o do Array
                'sExpectedValuesString = sExpectedValue & ", " & aExpectedValueArray(i)
                sExpectedValue = sExpectedValue & ", " & aExpectedValueArray(i)
            End If
        
        Next
    
    Application.ScreenUpdating = True
    
    '----------------------------------------------------------------------------------------------------------------------------------------------------------------

    If bolIsPossibleNullValue = True Then
        'sExpectedValuesString = sExpectedValuesString & ", NULL"
        sExpectedValue = sExpectedValue & "NULL"
    End If


'End If

'================================================================================================================================================================
'VERIFICA��O VALORES C�LULAS vs VALORES ESPERADOS

'Loop for iterando sobre todas as linhas do relat�rio

    Application.ScreenUpdating = False
    
        For lRow = iStartTestLine To Cells(200000, iForColumnNumber).End(xlUp).Row
        
            'Stop
            Cells(lRow, iSearchColumnNumber).Select
        
            'Verifica se o valor da c�lula � = "" ou = vazio e se � poss�vel valores nulos na c�lula
            If (Cells(lRow, iSearchColumnNumber).Value = "" Or IsEmpty(Cells(lRow, iSearchColumnNumber))) And bolIsPossibleNullValue = False Then
            
                'Stop
                Cells(lRow, iSearchColumnNumber).Select
                sTestStatus = "Failed"
                sRealValue = "NULL"
                Exit For
                        
            'Verifica se valores da c�lula est� contido dentro do array aExpectedValueArray ou se o valor � nulo
        
            ElseIf Not IsNumeric(Application.Match(Cells(lRow, iSearchColumnNumber).Value, aExpectedValueArray, 0)) _
                And (Cells(lRow, iSearchColumnNumber).Value <> "" _
                And Not IsEmpty(Cells(lRow, iSearchColumnNumber))) Then
            
                Stop
                Cells(lRow, iSearchColumnNumber).Select
                sTestStatus = "Failed"
                sRealValue = Cells(lRow, iSearchColumnNumber).Value
                Exit For
        
            End If
        
        Next
    
    Application.ScreenUpdating = True
    
    If sTestStatus = "Failed" Then
    
        Call FailedPatternOfCellsTests_Lib( _
            sExecutionID, _
            sUser, _
            iTestNumber, _
            sTestDescription, _
            iSearchColumnNumber, _
            lRow, _
            sExpectedValue, _
            sRealValue)
    Else
        
        sTestStatus = "Pass"
        'Stop
        'Call UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)
    End If

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub FailedPatternOfCellsTests_Lib( _
         sExecutionID, _
         sUser, _
         iTestNumber, _
         sTestDescription, _
         SearchColumnNumber, _
         lRow, _
         sExpectedValue, _
         sRealValue)


Dim sMessageboxExpectedLabel    As String
Dim sMessageboxFoundLabel       As String
Dim sString                     As String
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub FailedPatternOfCellsTests_Lib(iTestNumber, sTestDescription, SearchColumnNumber, lRow, sExpectedValue, sRealValue)"
sFunctionParameters = sExecutionID & " ," & sUser & " ," & iTestNumber & ", " & sTestDescription & ", " & SearchColumnNumber & ", " & lRow & ", " & sExpectedValue & ", " & sRealValue
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Cells(lRow, SearchColumnNumber).Interior.ColorIndex = 3
    Cells(lRow, SearchColumnNumber).Font.ColorIndex = 2
    Application.ScreenUpdating = True
    Cells(lRow, SearchColumnNumber).Select
    
    'Verifica se valor da c�lula � "" ou vazio
    If Cells(lRow, SearchColumnNumber).Value = "" Or _
          IsEmpty(Cells(lRow, SearchColumnNumber)) Then
          sRealValue = "NULL" 'atribui a sting "NULL" para a vari�vel RealValue que ser� chamada no MsgBox de falha de teste
    End If
    
    sCell = Cells(lRow, SearchColumnNumber).Address
    
    'Stop
    'Verificar se vari�vel sTestStatus ja n�o est� carregada atrav�z das fun��es
        'PatternSearchValuesInArrayLoopFor_Lib
    
    sTestStatus = "Failed"
    'Stop
    
    Call UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)
    
    Call InsertTestReturnErrors_Lib(sAutomationID, _
                                        sExecutionID, _
                                        sUser, _
                                        iTestNumber, _
                                        sTestDescription, _
                                        sExpectedValue, _
                                        sRealValue, _
                                        sCell)
                            
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub


Sub FailedPatternTests_Lib( _
         sExecutionID, _
         sUser, _
         iTestNumber, _
         sTestDescription, _
         sExpectedValue, _
         sRealValue)


Dim sMessageboxExpectedLabel    As String
Dim sMessageboxFoundLabel       As String
Dim sString                     As String
Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub FailedPatternTests_Lib(sExecutionID, sUser, iTestNumber, sTestDescription, sExpectedValue, sRealValue)"
sFunctionParameters = sExecutionID & " ," & sUser & " ," & iTestNumber & ", " & sTestDescription & ", " & sExpectedValue & ", " & sRealValue
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sTestStatus = "Failed"
    
    Stop
    Call UpdateExecTestStatus_Lib(sExecutionID, sUser, sTestStatus)
    
    Call InsertTestReturnErrors_Lib(sAutomationID, _
                                        sExecutionID, _
                                        sUser, _
                                        iTestNumber, _
                                        sTestDescription, _
                                        sExpectedValue, _
                                        sRealValue, _
                                        sCell)
                            
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

