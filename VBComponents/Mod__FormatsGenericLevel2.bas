Attribute VB_Name = "Mod__FormatsGenericLevel2"
Option Explicit


Sub FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader) 'Formata��o gen�rica para qualquer tabela

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub FormatGenericTable_Lib(sExecutionID, sUser, rn, rnHeader, rnWithoutHeader)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rn.Address & ", " & rnHeader.Address & ", " & rnWithoutHeader.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)


    Call FormatGenericFonts_Lib(sExecutionID, sUser, rn)
    
    If Not rnWithoutHeader Is Nothing Then
        Call FormatGenericBorders_Lib(sExecutionID, sUser, rnWithoutHeader)
    End If
    
    Call FormatGenericHeaderRange_Lib(sExecutionID, sUser, rnHeader)
    rn.EntireColumn.AutoFit
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub FormatGenericHeaderRange_Lib(sExecutionID, sUser, rnHeader) 'Gen�rico para todos os HEADER

Dim sFunctionName               As String
Dim sFunctionParameters         As String
Dim sExecutionExitMode          As String

sFunctionName = "Sub FormatGenericHeaderRange_Lib(sExecutionID, sUser, rnHeader)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & rnHeader.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    
    With rnHeader
        .Font.Bold = True
        .Font.ColorIndex = 2
        .Interior.ColorIndex = 16
        .HorizontalAlignment = xlCenter
        .EntireRow.RowHeight = 18
    End With
    
    rnHeader.EntireColumn.AutoFit
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)
       
End Sub

Sub FormatGenericSelectRange_Lib(sExecutionID, sUser, ParametroRecebido) 'Gen�rico para todos os select

Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub FormatGenericSelectRange_Lib(sExecutionID, sUser, ParametroRecebido)"
sFunctionParameters = sExecutionID & ", " & sUser & ", RANGE " & ParametroRecebido.Address
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    rnRange.ClearFormats
    
    Call FormatGenericBorders_Lib(ParametroRecebido)
    Call FormatGenericFonts_Lib(ParametroRecebido)
    Call FormatGenericGrayFonts_Lib(ParametroRecebido)
    Call FormatsGenericTextNumberFormat_Lib(ParametroRecebido)
    ParametroRecebido.EntireColumn.AutoFit
    ActiveWindow.LargeScroll Down:=-3
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub




