Attribute VB_Name = "Mod_Excluir"

Function VerifyExistsVcFBL5N_Lib()

    Stop

    wsZFI016.Select
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    If [A2].Value = "" Then
        temVcZFI016 = False
    Else
        [A1].CurrentRegion.AutoFilter Field:=21, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
        If ActiveSheet.AutoFilter.Range.Columns(1).SpecialCells(xlCellTypeVisible).Cells.Count - 1 >= 1 Then
            temVcZFI016 = True
        Else
            temVcZFI016 = False
        End If
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    End If
    
    wsFBL5N.Select
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    If [A2].Value = "" Then
        temVcFBL5N = False
    Else
        [A1].CurrentRegion.AutoFilter Field:=23, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
        If ActiveSheet.AutoFilter.Range.Columns(1).SpecialCells(xlCellTypeVisible).Cells.Count - 1 >= 1 Then
            temVcFBL5N = True
        Else
            temVcFBL5N = False
        End If
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    End If
    
    
    wsReavalia��oCambial.Select
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    If [A2].Value = "" Then
        temVcReavaliacaoCambial = False
    Else
        [A1].CurrentRegion.AutoFilter Field:=17, Criteria1:=">0", Operator:=xlOr, Criteria2:="<0"
        If ActiveSheet.AutoFilter.Range.Columns(1).SpecialCells(xlCellTypeVisible).Cells.Count - 1 >= 1 Then
            temVcReavaliacaoCambial = True
        Else
            temVcReavaliacaoCambial = False
        End If
    On Error Resume Next
        ActiveSheet.ShowAllData
    On Error GoTo 0
    End If


End Function

Function LoadGeneralDate_Lib(wsTemp)
    
    Stop
    
    wsTemp.[A1].Value = "Period"
    wsTemp.[B1].Value = "Year"
    wsTemp.[C1].Value = "Month"
    
    wsTemp.[A2].Value = dtPeriod
    wsTemp.[B2].Value = iYear
    wsTemp.[C2].Value = iMonth
    
    ActiveWorkbook.Names.Add Name:="vPeriod", RefersToR1C1:="=Temp!R2C1"
    ActiveWorkbook.Names.Add Name:="iYear", RefersToR1C1:="=Temp!R2C2"
    ActiveWorkbook.Names.Add Name:="iMonth", RefersToR1C1:="=Temp!R2C3"

End Function
