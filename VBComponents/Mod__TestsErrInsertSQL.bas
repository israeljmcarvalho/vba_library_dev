Attribute VB_Name = "Mod__TestsErrInsertSQL"
Option Explicit

Sub InsertTestReturnErrors_Lib(sAutomationID, _
                                    sExecutionID, _
                                    sUser, _
                                    iTestNumber, _
                                    sTestDescription, _
                                    sExpectedValue, _
                                    sRealValue, _
                                    sCell)
                                    
Dim sInsertQuery                    As String
Dim sWorkbookName                   As String
Dim sWorksheetName                  As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub InsertTestReturnErrors_Lib(sAutomationID, iTestNumber, sTestDescription, sExpectedValue, sRealValue, sCell)"
sFunctionParameters = sAutomationID & ", " & sUser & ", " & sExecutionID & ", " & iTestNumber & ", " & sTestDescription & ", " & sExpectedValue & ", " & sRealValue & ", " & sCell
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)
    
    sWorkbookName = ActiveWorkbook.Name
    sWorksheetName = ActiveSheet.Name
     
    If sCell = Empty Then
            sInsertQuery = "Insert Into T_TestsErrorsReturn " & _
        "Values (" & _
            "'" & sExecutionID & "', " & _
             "'" & sAutomationID & "', " & _
            "'" & iTestNumber & "', " & _
            "'" & sTestDescription & "', " & _
            "'" & sExpectedValue & "', " & _
            "'" & sRealValue & "', " & _
            "'" & sWorkbookName & "', " & _
            "'" & sWorksheetName & "', " & _
            "'-', " & _
            "GetDate(), " & _
            "'" & sUser & "'" & _
        ")"
        
    Else
        
        sInsertQuery = "Insert Into T_TestsErrorsReturn " & _
            "Values (" & _
                "'" & sExecutionID & "', " & _
                 "'" & sAutomationID & "', " & _
                "'" & iTestNumber & "', " & _
                "'" & sTestDescription & "', " & _
                "'" & sExpectedValue & "', " & _
                "'" & sRealValue & "', " & _
                "'" & sWorkbookName & "', " & _
                "'" & sWorksheetName & "', " & _
                "'" & sCell & "', " & _
                "GetDate(), " & _
                "'" & sUser & "'" & _
            ")"
        
    End If
    
    Call OpenSqlServerConection_Lib
    rs.Open sInsertQuery, conn
    Call CloseSqlServerConection_Lib

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

