Attribute VB_Name = "Mod__SqlServerInserts"
Option Explicit

Sub InsertOSParameters_Lib(sExecutionID, sUser, sHostName, aSOParameters)

Dim aArray()
Dim sValues                         As String
Dim sInsertHeader                   As String
Dim iCountOfParameters              As Integer
Dim sParameterIndex                 As String
Dim sParameterValue                 As String
Dim sSqlQuery                       As String
Dim sFunctionName                   As String
Dim sFunctionParameters             As String
Dim sExecutionExitMode              As String

sFunctionName = "Sub InsertOSParameters_Lib(sExecutionID, sUser, sHostName, aSOParameters)"
sFunctionParameters = sExecutionID & ", " & sUser & " ARRAY aSOParameters"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    iCountOfParameters = UBound(aSOParameters, 2)
    
    Call OpenSqlServerConection_Lib
    
    For i = 0 To iCountOfParameters
        sParameterIndex = aSOParameters(1, i)
        sParameterValue = Application.International(CLng(sParameterIndex))
        sSqlQuery = "Execute [SP_Insert_AutomationSOParameters] @ExecutionID = '" & sExecutionID & "' , @ParameterIndex = '" & sParameterIndex & "', @ParameterValue = '" & sParameterValue & "', @HostName = '" & sHostName & "', @User = '" & sUser & "'"
        rs.Open sSqlQuery, conn
        Set rs = Nothing
    Next
    
Call CloseSqlServerConection_Lib

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Function SendExecutionLog_Lib(sAutomationID, sExecutionID, sMoment, sFunctionName, sFunctionParameters, sExecutionExitMode, sUser)

Dim sSqlQuery               As String
    
sSqlQuery = "Execute SP_ExecutionFunctionLog " & _
        "@AutomationId = '" & sAutomationID & "', " & _
        "@ExecutionID = '" & sExecutionID & "', " & _
        "@Moment = '" & sMoment & "', " & _
        "@FunctionName = '" & sFunctionName & "', " & _
        "@FunctionParameters = '" & sFunctionParameters & "', " & _
        "@ExecutionExitMode = '" & sExecutionExitMode & "', " & _
        "@User = '" & sUser & "'"

Call OpenSqlServerConection_Lib
Set rs = conn.Execute(sSqlQuery)
Call CloseSqlServerConection_Lib

End Function

