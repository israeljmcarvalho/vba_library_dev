Attribute VB_Name = "Mod__VBComponets"

'----------------------------------------------------------------------------------------------
' MExamples
'----------------------------------------------------------------------------------------------
'
' Examples module for CodeProject article:
' Deconstructing a VBA code module
' Mark Regal 2013.08.08
'
' Supported Office Versions
' MS Office 2003 - 2013
' Does not support MS Outlook
'
' Installation
' Import into any VBE supported MS Office project
'
'----------------------------------------------------------------------------------------------

Option Explicit

'VBIDE.vbext_ComponentType
Private Const vbext_ct_ActiveXDesigner As Long = 11
Private Const vbext_ct_ClassModule As Long = 2
Private Const vbext_ct_Document As Long = 100
Private Const vbext_ct_MSForm As Long = 3
Private Const vbext_ct_StdModule As Long = 1

'VBIDE.vbext_ProcKind
Private Const vbext_pk_Get As Long = 3
Private Const vbext_pk_Let As Long = 1
Private Const vbext_pk_Set As Long = 2
Private Const vbext_pk_Proc As Long = 0

'VBIDE.vbext_RefKind
Private Const vbext_rk_Project As Long = 1
Private Const vbext_rk_TypeLib As Long = 0

'------------------------------------------
' ListCode
'------------------------------------------

Public Sub ListCode()

    Dim Component As Object
    Dim Index As Long
    
    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule
        
            Debug.Print Chr(13) & Chr(13) & String(66, "=")
            Debug.Print "Component.Name: " & Component.Name
            Debug.Print "CountOfDeclarationLines: " & Component.CodeModule.CountOfDeclarationLines
            Debug.Print "CountOfLines: " & Component.CodeModule.CountOfLines
            Debug.Print String(66, "=") & Chr(13) & Chr(13)
            
            'The Declarations
            For Index = 1 To .CountOfDeclarationLines
                Debug.Print .Lines(Index, 1)
                MsgBox Len(.Lines(Index, 1))
            Next Index

            'The Procedures

            
            For Index = .CountOfDeclarationLines + 1 To .CountOfLines
                Debug.Print .Lines(Index, 1)
            Next Index

        End With

    Next Component

End Sub

'------------------------------------------
' ListNames
'------------------------------------------

Public Sub ListNames()

    Dim Component As Object
    Dim Name As String
    Dim Kind As Long
    Dim Index As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule

            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines
                Name = .ProcOfLine(Index, Kind)
                Debug.Print Component.Name & "." & Name
                Index = .ProcStartLine(Name, Kind) + .ProcCountLines(Name, Kind) + 1
            Loop

        End With

    Next Component

End Sub

'------------------------------------------
' GetProcedureCount
'------------------------------------------

Public Function GetProcedureCount() As Long

    Dim Component As Object
    Dim Kind As Long
    Dim Name As String
    Dim Index As Long
    Dim Count As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents
            
            Debug.Print Application.VBE.ActiveVBProject.Filename
            
            
            
        With Component.CodeModule
        
            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines
                Count = Count + 1
                Name = .ProcOfLine(Index, Kind)
                Index = .ProcStartLine(Name, Kind) + .ProcCountLines(Name, Kind) + 1
            Loop
            
        End With
    
    Next Component
    
    GetProcedureCount = Count

End Function

'------------------------------------------
' GetProcKind
'------------------------------------------

Public Function GetProcKind(Kind As Long, Declaration As String) As String

    'Convert the procedure kind to text
    Select Case Kind

        Case vbext_pk_Get
            GetProcKind = "Get"

        Case vbext_pk_Let
            GetProcKind = "Let"

        Case vbext_pk_Set
            GetProcKind = "Set"

        'Best Guess
        Case vbext_pk_Proc
            If InStr(1, Declaration, "Function ", vbBinaryCompare) > 0 Then
                GetProcKind = "Func"
            Else
                GetProcKind = "Sub"
            End If

        Case Else
            GetProcKind = "Undefined"

    End Select

End Function

'------------------------------------------
' ReportProcNames
'------------------------------------------

Public Sub ReportProcNames()

    Dim Component As Object
    Dim Name As String
    Dim Kind As Long
    Dim Start As Long
    Dim Body As Long
    Dim Length As Long
    Dim BodyLines As Long
    Dim Declaration As String
    Dim ProcedureType As String
    Dim Index As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule

            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines

                Debug.Print Index

                Name = .ProcOfLine(Index, Kind)
                Start = .ProcStartLine(Name, Kind)
                Body = .ProcBodyLine(Name, Kind)
                Length = .ProcCountLines(Name, Kind)
                BodyLines = Length - (Body - Start)
                Declaration = Trim(.Lines(Body, 1))
                ProcedureType = GetProcKind(Kind, Declaration)
                
                'Debug.Print Component.CodeModule.ProcOfLine(Index, Kind)
                
                'Stop
                'Debug.Print "Component.Name: " & Component.Name
                'Debug.Print "Name: " & Name
                'Debug.Print "ProcedureType: " & ProcedureType
                'Debug.Print CStr(BodyLines)
                
                Debug.Print Declaration
                
                'Debug.Print Component.Name & "." & Name & "." & ProcedureType & "." & CStr(BodyLines)
                Index = Start + Length + 1
                
            Loop

        End With

    Next Component

End Sub


Sub VB_Project()

Dim objVBPrj As VBIDE.VBProject
Dim objVBCom As VBIDE.VBComponent
Dim vbrRef As VBIDE.Reference

' Create new workbook Application.Workbooks.Add

' Create a new module in a workbook
'Application.VBE.ActiveVBProject.VBComponents.Add (vbext_ct_StdModule)

' List VBA projects as well as references and ' component names they contain

For Each objVBPrj In Application.VBE.VBProjects

'    Debug.Print Chr(13) & String(66, "=") & Chr(13)
'    Debug.Print "objVBPrj.Name  = " & objVBPrj.Name
    
'    For Each vbrRef In objVBPrj.References
'        With vbrRef
'            Debug.Print vbTab & .Name & "---" & .FullPath
'        End With
'    Next
    

    Debug.Print
    
    For Each objVBCom In objVBPrj.VBComponents
    
        'MsgBox objVBCom.Name
    
        If objVBCom.Name = "Mod__GeneralFuntions" Then
            'Stop
            Debug.Print vbTab & objVBCom.Name
            Debug.Print vbTab & objVBCom.CodeModule.CountOfLines
            Debug.Print vbTab & objVBCom.CodeModule.CountOfDeclarationLines
            'Debug.Print vbTab & objVBCom.modul
            'Debug.Print vbTab & objVBCom.CodeModule.ProcBodyLine("Function",vbext_pk_Let vbext_pk_Get)
        End If
    Next

Next

' Export the entire Modulel in the activeVB project to disk

Stop

With ThisWorkbook.VBProject.VBComponents("Module1")

    If MsgBox("Module1 contains " & .CodeModule.CountOfLines & " lines." & vbCrLf & "Do you want to export it to a file?", vbYesNo) = vbYes Then
        .Export "C:\Users\icarvalho\OneDrive - Lear Corporation\VBA\Conciliações\Contas a Pagar\Modules\myCode1.bas"
    End If

End With


End Sub



Sub ListVBComponents_Lib()

Dim objVBPrj            As VBIDE.VBProject
Dim objVBCom            As VBIDE.VBComponent
Dim vbrRef              As VBIDE.Reference

Dim iCharPosition       As Integer
Dim sString             As String
Dim sChar               As String

' List VBA projects as well as references and ' component names they contain

For Each objVBPrj In Application.VBE.VBProjects

    If objVBPrj.Name = "VBAProject" Then
    
        Debug.Print Chr(13) & String(140, "=") & Chr(13)
        
        sString = objVBPrj.Filename
        sChar = "\"
        iCharPosition = GetLastCharPosition_Lib(sString:=objVBPrj.Filename, sChar:="\")
        Debug.Print "FileName  = " & Mid(objVBPrj.Filename, iCharPosition)
        Debug.Print "objVBPrj.Name  = " & objVBPrj.Name
        Debug.Print "objVBPrj.Name  = " & objVBPrj.Filename
        

    
        For Each vbrRef In objVBPrj.References
            With vbrRef
                Debug.Print
                Debug.Print vbTab & "References.Description = " & .Description
                Debug.Print vbTab & "References.Name = " & .Name
                Debug.Print vbTab & "References.FullPath = " & .FullPath
                Debug.Print vbTab & "References.GUID = " & .Guid
                Debug.Print
            End With
        Next
    
    Debug.Print Chr(13) & String(140, "=") & Chr(13)
    
        For Each objVBCom In objVBPrj.VBComponents
            Debug.Print
            Debug.Print vbTab & "objVBCom.Name = " & objVBCom.Name
            Debug.Print vbTab & vbTab & "objVBCom.CodeModule.CountOfLines = " & objVBCom.CodeModule.CountOfLines
            Debug.Print vbTab & vbTab & "objVBCom.CodeModule.CountOfDeclarationLines = " & objVBCom.CodeModule.CountOfDeclarationLines
            Debug.Print
        Next
    
    End If

Next


End Sub

Sub DeleteAllModules()

Dim szImportPath
Dim objFSO
Dim oFolder

'------------------------------------------------------------------------------------------------------------------------------------------------------------------
'Loop through all files in a folder

'Set objFSO = CreateObject("Scripting.FileSystemObject")
'Set oFolder = objFSO.GetFolder("C:\Users\icarvalho\Documents\VBA\Git\fagll03_vendor\VBComponents2")

'For Each oFile In oFolder.Files
'    Debug.Print oFile.Name
'Next

'------------------------------------------------------------------------------------------------------------------------------------------------------------------
'Loop through all VbComponents

Dim Component As Object
Dim Name As String
Dim Kind As Long
Dim Index As Long
Dim objVBPrj                    As VBIDE.VBProject
Dim objVBCom            As VBIDE.VBComponent
Dim wb

Set wb = ThisWorkbook
Set objVBPrj = wb.VBProject

    'For Each Component In Application.VBE.ActiveVBProject.VBComponents
    
    'Debug.Print objVBPrj.Name
    
    For Each objVBCom In objVBPrj.VBComponents
        'Case "100" = Microsoft Excel Objects. Sheet modules or the ThisWorkbook module.
        'Case "1" = Regular Modules (.bas)
        'Case "2" = Class Modules
        'Case "3" = 'UserForms
        'Debug.Print CStr(objVBCom.Type) & " - " & CStr(objVBCom.Name)
        
        If CStr(objVBCom.Type) = "3" Then
        
            'If CStr(objVBCom.Name) = "Module7" Then
                Debug.Print CStr(objVBCom.Type) & " - " & CStr(objVBCom.Name)
                ActiveWorkbook.VBProject.VBComponents.Remove ActiveWorkbook.VBProject.VBComponents(objVBCom.Name)
            'End If
            
        End If
        
    Next objVBCom
            
End

