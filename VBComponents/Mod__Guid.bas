Attribute VB_Name = "Mod__Guid"
Option Explicit

Dim strGuid As String

Private Type GUID_TYPE

Data1 As Long
Data2 As Integer
Data3 As Integer
Data4(7) As Byte

End Type

Private Declare PtrSafe Function CoCreateGuid Lib "ole32.dll" (Guid As GUID_TYPE) As LongPtr
Private Declare PtrSafe Function StringFromGUID2 Lib "ole32.dll" (Guid As GUID_TYPE, ByVal lpStrGuid As LongPtr, ByVal cbMax As Long) As LongPtr

Function CreateGuidString_Lib()

Dim Guid As GUID_TYPE
Dim strGuid As String
Dim retValue As LongPtr

Const guidLength As Long = 39 'registry GUID format with null terminator {xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx}

    retValue = CoCreateGuid(Guid)

    If retValue = 0 Then

        strGuid = String$(guidLength, vbNullChar)
        retValue = StringFromGUID2(Guid, StrPtr(strGuid), guidLength)

        If retValue = guidLength Then
            CreateGuidString_Lib = strGuid
            'Debug.Print CreateGuidString_Lib & String(5, " ") & " Len: " & Len(CreateGuidString_Lib)
            CreateGuidString_Lib = replace(strGuid, "{", "")
            'Debug.Print CreateGuidString_Lib & String(5, " ") & " Len: " & Len(CreateGuidString_Lib)
            CreateGuidString_Lib = replace(CreateGuidString_Lib, "}", "")
            'Debug.Print CreateGuidString_Lib & String(5, " ") & " Len: " & Len(CreateGuidString_Lib)
            CreateGuidString_Lib = Left(CreateGuidString_Lib, 36)
            'Debug.Print CreateGuidString_Lib & String(5, " ") & " Len: " & Len(CreateGuidString_Lib)
        End If

    End If

End Function


