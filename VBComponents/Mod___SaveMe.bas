Attribute VB_Name = "Mod___SaveMe"
Option Explicit

Sub SaveMe()

ThisWorkbook.Save

End Sub

Sub ExportVbComponents()

Dim wb                          As Workbook
Dim objVBPrj                    As VBIDE.VBProject
Dim sAutomationsRootPath        As String
Dim sAutomationGitRepository    As String
Dim sUser                       As String
Dim sVBComponentsPath           As String
Dim sGitRepositoryPath          As String
Dim sFilesToBeKilled            As String
Dim sRootPath                   As String
Dim sAutomationMode             As String

sAutomationID = SetAutomationID
sUser = LCase(GetUserName_Lib)
'sVBComponentsPath = "C:\Users\icarvalho\OneDrive - Lear Corporation\VBA\Git\vba_library_dev\VBComponents"
sVBComponentsPath = "C:\Users\icarvalho\Documents\VBA\Git\vba_library_dev\VBComponents"
sFilesToBeKilled = sVBComponentsPath & "\*.*"

On Error Resume Next
    Kill sFilesToBeKilled
On Error GoTo 0

Set wb = ThisWorkbook
Set objVBPrj = wb.VBProject
'Set objVBPrj = Application.VBE.ActiveVBProject
Call ExportVbComponents_Lib(wb, sVBComponentsPath, objVBPrj)


End Sub

Function SetAutomationID() As String

SetAutomationID = "7AF890A2-1653-4220-BC45-24FC416E512B"
'Select * from T_Automations Where [ID] = '7AF890A2-1653-4220-BC45-24FC416E512B'

End Function


Function ExportVbComponents_Anterior()

Dim wb                          As Workbook
Dim objVBPrj                    As VBIDE.VBProject
Dim sVBComponentsPath           As String
Dim sGitRepositoryPath          As String
Dim sFilesToBeKilled            As String

ThisWorkbook.Save
sAutomationID = SetAutomationID
sUser = GetUserName_Lib
sAutomationsRootPath = "C:\Users\" & sUser & cDevDefaultPath
sAutomationGitRepository = GetAutomationGitRepository_Lib(sAutomationID, sUser)
sOriginalFilesPath = sAutomationsRootPath & sAutomationGitRepository & "\" & cOriginalFilesFolder
sVBComponentsPath = sAutomationsRootPath & sAutomationGitRepository & "\" & cVBComponentsFolder
sFilesToBeKilled = sVBComponentsPath & "\*.*"
Call KillFiles_Lib(sFilesToBeKilled)

Set wb = ThisWorkbook
Set objVBPrj = wb.VBProject

Call ExportVbComponents_Lib(wb, sVBComponentsPath, objVBPrj)

End Function

