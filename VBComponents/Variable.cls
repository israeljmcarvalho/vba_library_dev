VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Variable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'declare the fields that will be attached to every instance of this class

Public Name As String '<--| this will store the name of the variable to which you'll set the object of this class
Public Value As Variant '<--| this will store the value associated with the variable to which you'll set the object of this class

'declare a `method` to write the `value` of the object in the named range named after the `name` field

Sub WriteRange(Optional wb As Variant) '<--| you can pass the reference of the workbook whose named ranges you want to exploit
   
   If IsMissing(wb) Then Set wb = ActiveWorkbook '<--| if no workbook reference is passed then the currently active workbook is assumed
   
   If TypeName(wb) = "Workbook" Then '<-- check for a proper workbook reference being passed)
        On Error Resume Next '<-- to prevent unassigned named range throw an error
        wb.Names(Name).RefersToRange.Value = Value '<--| write the  value of the `value` filed of the current instance in the named range of the passed workbook named after the `name` filed of the current instance
    End If

End Sub
