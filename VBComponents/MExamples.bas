Attribute VB_Name = "MExamples"

'----------------------------------------------------------------------------------------------
' MExamples
'----------------------------------------------------------------------------------------------
'
' Examples module for CodeProject article:
' Deconstructing a VBA code module
' Mark Regal 2013.08.08
'
' Supported Office Versions
' MS Office 2003 - 2013
' Does not support MS Outlook
'
' Installation
' Import into any VBE supported MS Office project
'
'----------------------------------------------------------------------------------------------

Option Explicit

'VBIDE.vbext_ComponentType
Private Const vbext_ct_ActiveXDesigner As Long = 11
Private Const vbext_ct_ClassModule As Long = 2
Private Const vbext_ct_Document As Long = 100
Private Const vbext_ct_MSForm As Long = 3
Private Const vbext_ct_StdModule As Long = 1

'VBIDE.vbext_ProcKind
Private Const vbext_pk_Get As Long = 3
Private Const vbext_pk_Let As Long = 1
Private Const vbext_pk_Set As Long = 2
Private Const vbext_pk_Proc As Long = 0

'VBIDE.vbext_RefKind
Private Const vbext_rk_Project As Long = 1
Private Const vbext_rk_TypeLib As Long = 0

'------------------------------------------
' ListCode
'------------------------------------------

Public Sub ListCode_Rever()

    Dim Component As Object
    Dim Index As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule

Debug.Print Chr(13) & Chr(13) & String(66, "=")
Debug.Print "Component.Name: " & Component.Name
Debug.Print "CountOfDeclarationLines: " & Component.CodeModule.CountOfDeclarationLines
Debug.Print "CountOfLines: " & Component.CodeModule.CountOfLines
Debug.Print String(66, "=") & Chr(13) & Chr(13)

'The Declarations
For Index = 1 To .CountOfDeclarationLines
    Debug.Print .Lines(Index, 1)
    MsgBox Len(.Lines(Index, 1))
            Next Index

            'The Procedures

            
            For Index = .CountOfDeclarationLines + 1 To .CountOfLines
                Debug.Print .Lines(Index, 1)
            Next Index

        End With

    Next Component

End Sub

'------------------------------------------
' ListNames
'------------------------------------------

Public Sub ListNames_Rever()

    Dim Component As Object
    Dim Name As String
    Dim Kind As Long
    Dim Index As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule

            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines
                Name = .ProcOfLine(Index, Kind)
                Debug.Print Component.Name & "." & Name
                Index = .ProcStartLine(Name, Kind) + .ProcCountLines(Name, Kind) + 1
            Loop

        End With

    Next Component

End Sub

'------------------------------------------
' GetProcedureCount
'------------------------------------------

Public Function GetProcedureCount_Rever() As Long

    Dim Component As Object
    Dim Kind As Long
    Dim Name As String
    Dim Index As Long
    Dim Count As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents
            
        With Component.CodeModule
        
            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines
                Count = Count + 1
                Name = .ProcOfLine(Index, Kind)
                Index = .ProcStartLine(Name, Kind) + .ProcCountLines(Name, Kind) + 1
            Loop
            
        End With
    
    Next Component
    
    GetProcedureCount = Count

End Function

'------------------------------------------
' GetProcKind
'------------------------------------------

Public Function GetProcKind_Rever(Kind As Long, Declaration As String) As String

    'Convert the procedure kind to text
    Select Case Kind

        Case vbext_pk_Get
            GetProcKind = "Get"

        Case vbext_pk_Let
            GetProcKind = "Let"

        Case vbext_pk_Set
            GetProcKind = "Set"

        'Best Guess
        Case vbext_pk_Proc
            If InStr(1, Declaration, "Function ", vbBinaryCompare) > 0 Then
                GetProcKind = "Func"
            Else
                GetProcKind = "Sub"
            End If

        Case Else
            GetProcKind = "Undefined"

    End Select

End Function

'------------------------------------------
' ReportProcNames
'------------------------------------------

Public Sub ReportProcNames_Rever()

    Dim Component As Object
    Dim Name As String
    Dim Kind As Long
    Dim Start As Long
    Dim Body As Long
    Dim Length As Long
    Dim BodyLines As Long
    Dim Declaration As String
    Dim ProcedureType As String
    Dim Index As Long

    For Each Component In Application.VBE.ActiveVBProject.VBComponents

        With Component.CodeModule

            'The Procedures
            Index = .CountOfDeclarationLines + 1
            
            Do While Index < .CountOfLines

                Name = .ProcOfLine(Index, Kind)
                Start = .ProcStartLine(Name, Kind)
                Body = .ProcBodyLine(Name, Kind)
                Length = .ProcCountLines(Name, Kind)
                BodyLines = Length - (Body - Start)
                Declaration = Trim(.Lines(Body, 1))
                ProcedureType = GetProcKind(Kind, Declaration)
                Debug.Print Component.Name & "." & Name & "." & ProcedureType & "." & CStr(BodyLines)
                Index = Start + Length + 1
                
            Loop

        End With

    Next Component

End Sub

