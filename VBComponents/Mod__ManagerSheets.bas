Attribute VB_Name = "Mod__ManagerSheets"
Option Explicit

Sub DeleteSheets_Lib(aArray)

Dim i As Integer

For i = 0 To UBound(aArray)

    sSheetName = aArray(i, 0)
    
    Application.DisplayAlerts = False
    On Error Resume Next
    
    Sheets(sSheetName).Delete
    
    On Error GoTo 0
    Application.DisplayAlerts = True

Next

End Sub


Sub DeleteSheetsExcept_Lib(sExecutionID, sUser, ws)

Dim wsWorksheet As Worksheet
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub DeleteSheetsExcept_Lib(sExecutionID, sUser, ws)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & "SHEET " & ws.Name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

Application.DisplayAlerts = False

For Each wsWorksheet In ActiveWorkbook.WorkSheets
    If wsWorksheet.Name <> ws.Name Then
       wsWorksheet.Delete
    End If
Next wsWorksheet

Application.DisplayAlerts = True

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub

Sub DeleteSheetsExcept2_Lib(sExecutionID, sUser, aSheetsAPreservar)

Dim wsWorksheet              As Worksheet
Dim x
Dim y                        As Worksheet
Dim iExcluir                 As Integer
Dim sFunctionName            As String
Dim sFunctionParameters      As String
Dim sExecutionExitMode       As String

sFunctionName = "Sub DeleteSheetsExcept2_Lib(sExecutionID, sUser, aSheetsAPreservar)"
sFunctionParameters = sExecutionID & ", " & sUser & ", ARRAY aSheetsAPreservar"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    For Each y In WorkSheets
    
    iExcluir = 0
    
        For Each x In aSheetsAPreservar
            'Debug.Print y.Name & " / " & x & Chr(13)
            If y.Name = x Then
               iExcluir = 0
            Exit For
            Else
               iExcluir = iExcluir + 1
            End If
        Next x
        
        If iExcluir > 0 Then
            Application.DisplayAlerts = False
            y.Delete
            Application.DisplayAlerts = False
        End If
        'Debug.Print
        
    Next y

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub CreateSheetsFromArray_Lib(sExecutionID, sUser, aSheetsArray)

Dim i                                   As Integer
Dim sSheetColor                         As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub CreateSheetsFromArray_Lib(sExecutionID, sUser, sSheetsArray)"
sFunctionParameters = sExecutionID & ", " & sUser & ", ARRAY sSheetsArray"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

For i = 0 To UBound(aSheetsArray)

    sSheetName = aSheetsArray(i, 0)
    sSheetColor = aSheetsArray(i, 1)
    
    Sheets.Add(After:=Sheets(1)).Name = sSheetName
    
    Sheets(sSheetName).Activate

    With ActiveWorkbook.ActiveSheet.Tab
        .Color = sSheetColor
        .TintAndShade = 0
    End With
        
Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub CreateSheet_Lib(sExecutionID, sUser, sSheetName, wsAfterThisSheet)

Dim sFunctionName                    As String
Dim sFunctionParameters              As String
Dim sExecutionExitMode               As String

sFunctionName = "Sub CreateSheet_Lib(sExecutionID, sUser, sSheetName, wsAfterThisSheet)"
sFunctionParameters = sExecutionID & ", " & sUser & ", " & sSheetName & ", SHEET" & wsAfterThisSheet.Name
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    Sheets.Add After:=wsAfterThisSheet
    ActiveSheet.Name = sSheetName
    Sheets(sSheetName).Select

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub

Sub MoveSheets_Lib(sExecutionID, sUser, aSheetsArray)

Dim i                                   As Integer
Dim sOrigemSheet                        As String
Dim sTargetSheet                        As String
Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub MoveSheets_Lib(sExecutionID, sUser, aSheetsArray)"
sFunctionParameters = sExecutionID & ", " & sUser & ", ARRAY aSheetsArray"
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    For i = 0 To UBound(aSheetsArray)
    
        sOrigemSheet = aSheetsArray(i, 0)
        sTargetSheet = aSheetsArray(i, 1)
        
        If sTargetSheet = "0" Then
            Sheets(sOrigemSheet).Move Before:=Sheets(1)
        Else
            Sheets(sOrigemSheet).Move After:=Sheets(sTargetSheet)
        End If
    
    Next

sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)


End Sub

Sub Reexibir_Lib(sExecutionID, sUser)

Dim sFunctionName                       As String
Dim sFunctionParameters                 As String
Dim sExecutionExitMode                  As String

sFunctionName = "Sub Reexibir_Lib(sExecutionID, sUser)"
sFunctionParameters = sExecutionID & ", " & sUser
Call PrintInFunction_Lib(sExecutionID, sUser, sFunctionName, sFunctionParameters)

    For Each ws In WorkSheets
      
        ws.Select
        ws.[1:500].EntireRow.Hidden = False
        ws.[A:L].EntireColumn.Hidden = False
    
    Next ws
    
    ws2.Select
    
sExecutionExitMode = "Traditional"
Call PrintOutFunction_Lib(sExecutionID, sUser, sExecutionExitMode, sFunctionName, sFunctionParameters)

End Sub










